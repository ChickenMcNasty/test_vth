using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Globalization;
using System.Runtime.InteropServices;


namespace MainForm
{
    public partial class Form1 : Form
    {

        public string TestVersion = "2.0.18";

        Thread testingAnimationThread;

        Thread myFunctionalTestThread;

        private string serialLogText = null;

        private string testLogText = null;

        public Form1()
        {
            InitializeComponent();
            this.comboBox1.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames()); //populate with available COM ports 
            this.label3.Text = "Test Program v" + this.TestVersion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!VTH.FunctionalTest.busy)
            {
                this.CreateFunctionalTestObject();
            }
        }

        private void CheckKeys(object sender, KeyPressEventArgs e) //Monitor for enter keypress
        {
            if ((e.KeyChar == (char)Keys.Return)&&(!VTH.FunctionalTest.busy))  //If enter key is pressed
            {
                e.Handled = true;
                this.CreateFunctionalTestObject();
            }
        }

        private void CreateFunctionalTestObject()
        {
            this.ClearLogBoxes();
            try { myFunctionalTestThread.Join();}
            catch { }
            VTH.FunctionalTest myTest = new VTH.FunctionalTest(this);  //Create/run the functional Test object
            myFunctionalTestThread = new Thread(myTest.StartTest);
            myFunctionalTestThread.Start();
        }

        public string GetSerNum() //Grab the serial number from the text box as a string
        {
            //return this.textBox1.Text;
            return "0";
        }

        public void SetProgressBar(int setting)
        {
            this.progressBar1.Value = setting;
        }

        public void IncrementProgressBar()
        {
            this.progressBar1.Value += 1;
        }

        public void ClearLogBoxes()
        {
            this.textBox2.Clear();
            this.textBox3.Clear();
        }
        
        public void WriteSerialLog(string text)
        {
            /*if ((this.textBox2.Text.Length + text.Length) < this.textBox2.MaxLength)
            {
                this.textBox2.AppendText(text);
            }
            else
            {
                this.textBox2.SelectionStart = 0;
                this.textBox2.SelectionLength = (this.textBox2.Text.Length + text.Length) - this.textBox2.MaxLength;
                this.textBox2.SelectedText = "";
                this.textBox2.AppendText(text);
            }*/
            this.textBox2.AppendText(text);
            serialLogText += text;
        }

        public void WriteSerialLogFileToDisk()
        {
            StreamWriter serialLogSW = File.AppendText("VTH_SERIAL_LOG.TXT");
            serialLogSW.Write(serialLogText);
            serialLogSW.Close();
            serialLogText = null;
        }

        public void BackspaceSerialLogBox(int spaces)
        {
            this.textBox2.SelectionStart = this.textBox2.TextLength - spaces;
            this.textBox2.SelectionLength = spaces;
            this.textBox2.SelectedText = "";
            serialLogText = serialLogText.Substring(0, serialLogText.Length - spaces);
        }

        public void WriteTestLog(string text)
        {
            this.textBox3.AppendText(text);
            testLogText += text;
        }

        public void WriteTestLogFileToDisk()
        {
            StreamWriter testLogSW = File.AppendText("VTH_TEST_LOG.TXT");
            testLogSW.Write(testLogText);
            testLogSW.Close();
            testLogText = null;
        }

        public void StatusTesting() //Calls animation in separate thread
        {
            try { testingAnimationThread.Join(); }
            catch { }
            testingAnimationThread = new Thread(this.StatusTestingAnimation);
            testingAnimationThread.Start();
        }

        private void StatusTestingAnimation() //Big Grey Testing... Animation Box
        {
            System.Media.SoundPlayer myPlayer = new System.Media.SoundPlayer();
            myPlayer.SoundLocation = @"sounds\testing.wav";
            try { myPlayer.Play(); }
            catch { }
            this.label7.BackColor = System.Drawing.Color.LightGray;
            this.label7.Refresh();  //Redraw the PASS/FAIL textbox
            this.label7.Text = "T";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Te";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Tes";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Test";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Testi";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Testin";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Testing";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Testing.";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Testing..";
            System.Threading.Thread.Sleep(100);
            this.label7.Text = "Testing...";
            this.label7.Refresh();  //Redraw the PASS/FAIL textbox
        }

        public void StatusPass() //Big Green PASS box
        {
            testingAnimationThread.Join();
            System.Media.SoundPlayer myPlayer = new System.Media.SoundPlayer();
            myPlayer.SoundLocation = @"sounds\pass.wav";
            try { myPlayer.Play(); }
            catch { }
            this.label7.BackColor = System.Drawing.Color.Lime;
            this.label7.Text = "PASS";
            this.label7.Refresh();  //Redraw the PASS/FAIL textbox
            //this.IncrementMAC();
        }

        public void StatusFail() //Big Red FAIL Box
        {
            testingAnimationThread.Join();
            System.Media.SoundPlayer myPlayer = new System.Media.SoundPlayer();
            myPlayer.SoundLocation = @"sounds\fail.wav";
            try { myPlayer.Play(); }
            catch { }
            this.label7.BackColor = System.Drawing.Color.Red;
            this.label7.Text = "FAIL";
            this.label7.Refresh();  //Redraw the PASS/FAIL textbox
        }

        public void StatusError(string errorMessage) //Big Yellow ERROR Box
        {
            testingAnimationThread.Join();
            System.Media.SoundPlayer myPlayer = new System.Media.SoundPlayer();
            myPlayer.SoundLocation = @"sounds\error.wav";
            try { myPlayer.Play(); }
            catch { }
            this.label7.BackColor = System.Drawing.Color.Yellow;
            this.label7.Text = "ERROR";
            this.label7.Refresh();  //Redraw the PASS/FAIL textbox
            MessageBox.Show(errorMessage);
        } 

        public void ReturnFocus() //Blanks the serial number text box and returns focus there
        {
            //this.textBox4.Text = "";  //Blank the serial number text box
            this.textBox4.Select();
            this.textBox4.Focus();    //Make sure the serial number text box has focus            
        }
        
        public void DisableStartButton()
        {
            this.button1.Enabled = false;
            this.button2.Enabled = false;
            this.button3.Enabled = false;
            this.checkBox1.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            this.comboBox1.Enabled = false;
            this.comboBox2.Enabled = false;
            this.numericUpDown1.Enabled = false;
            this.textBox4.Enabled = false;
            this.textBox4.ReadOnly = true;
            this.ControlBox = false;
        }

        public void EnableStartButton()
        {
            this.button1.Enabled = true;
            this.button2.Enabled = true;
            this.button3.Enabled = true;
            this.checkBox1.Enabled = true;
            this.Cursor = Cursors.Default;
            this.comboBox1.Enabled = true;
            this.comboBox2.Enabled = true;
            this.numericUpDown1.Enabled = true;
            this.textBox4.Enabled = true;
            this.textBox4.ReadOnly = false;
            this.ControlBox = true;
        }

        public byte[] GetMAC()
        {
            string macString = this.textBox4.Text.Replace(" ", string.Empty);
            byte[] byteMAC = new byte[6];
            byteMAC[0] = (byte)(long.Parse(this.textBox4.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture) >> 40);
            byteMAC[1] = (byte)(long.Parse(this.textBox4.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture) >> 32);
            byteMAC[2] = (byte)(long.Parse(this.textBox4.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture) >> 24);
            byteMAC[3] = (byte)(long.Parse(this.textBox4.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture) >> 16);
            byteMAC[4] = (byte)(long.Parse(this.textBox4.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture) >> 8);
            byteMAC[5] = (byte)(long.Parse(this.textBox4.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture));
            return byteMAC;
        }

        public byte GetSubType()
        {
            if (this.comboBox2.SelectedIndex == 0) return 0x01;
            else if (this.comboBox2.SelectedIndex == 1) return 0x02;
            else if (this.comboBox2.SelectedIndex == 2) return 0x03;
            else return 0x00;
        }
        
        public void ReformatMAC()
        {
            long longMAC = long.Parse(this.textBox4.Text.Replace(" ", string.Empty), NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            if (longMAC > (281474976710655)) longMAC = 0;
            this.textBox4.Text = Convert.ToString(longMAC, 16).PadLeft(12, '0').Insert(2, " ").Insert(5, " ").Insert(8, " ").Insert(11, " ").Insert(14, " ");
            this.textBox4.Refresh();
        }

        public void IncrementMAC()
        {
            long longMAC = long.Parse(this.textBox4.Text.Replace(" ", string.Empty), NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            longMAC += 1;
            if (longMAC > (281474976710655)) longMAC = 0;
            this.textBox4.Text = Convert.ToString(longMAC, 16).PadLeft(12, '0').Insert(2, " ").Insert(5, " ").Insert(8, " ").Insert(11, " ").Insert(14, " ");
            this.textBox4.Refresh();
        }

        public string GetCOMPort()
        {
            return this.comboBox1.Text;
        }

        public string GetBaudRate()
        {
            return "115200";
        }

        public void PrintIfNecessary()
        {
            if (this.checkBox1.Checked)
            {
                this.PrintLabels();
            }
        }

        public void PrintLabels()
        {
            this.ReformatMAC();
            string s = "^XA~SD25^MD0^PW300^LH20,20^FO0,0^BXN,4,200^FD" + this.textBox4.Text.Replace(" ", string.Empty) + "^FS^FO100,00^ADN,10,4^FDMAC ADDRESS^FS^FO60,28^ADN,30,10^FD" + this.textBox4.Text + "^FS^XZ"; // device-dependent string, need a FormFeed?

            for (int i = 0; i < this.numericUpDown1.Value; i++) RawPrinterHelper.SendStringToPrinter(printDialog1.PrinterSettings.PrinterName, s);
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            printDialog1.PrinterSettings = new PrinterSettings();
            if (DialogResult.OK != printDialog1.ShowDialog(this))
            {
                MessageBox.Show("You must select a printer to print MAC address labels!");
            }

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.PrintLabels();
        }



    }

    public class RawPrinterHelper
    {
        // Structure and API declarions:
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        // SendBytesToPrinter()
        // When the function is given a printer name and an unmanaged array
        // of bytes, the function sends those bytes to the print queue.
        // Returns true on success, false on failure.
        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        {
            Int32 dwError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false; // Assume failure unless you specifically succeed.
            di.pDocName = "My C#.NET RAW Document";
            di.pDataType = "RAW";

            // Open the printer.
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Start a document.
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Start a page.
                    if (StartPagePrinter(hPrinter))
                    {
                        // Write your bytes.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }
            // If you did not succeed, GetLastError may give more information
            // about why not.
            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            // Open the file.
            FileStream fs = new FileStream(szFileName, FileMode.Open);
            // Create a BinaryReader on the file.
            BinaryReader br = new BinaryReader(fs);
            // Dim an array of bytes big enough to hold the file's contents.
            Byte[] bytes = new Byte[fs.Length];
            bool bSuccess = false;
            // Your unmanaged pointer.
            IntPtr pUnmanagedBytes = new IntPtr(0);
            int nLength;

            nLength = Convert.ToInt32(fs.Length);
            // Read the contents of the file into the array.
            bytes = br.ReadBytes(nLength);
            // Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
            // Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
            // Send the unmanaged bytes to the printer.
            bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
            // Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes);
            return bSuccess;
        }

        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;
            Int32 dwCount;

            // How many characters are in the string?
            // Fix from Nicholas Piasecki:
            // dwCount = szString.Length;
            dwCount = (szString.Length + 1) * Marshal.SystemMaxDBCSCharSize;

            // Assume that the printer is expecting ANSI text, and then convert
            // the string to ANSI text.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Send the converted ANSI string to the printer.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }


    }
}
