﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VTH
{
    public partial class Form2 : Form
    {
        FunctionalTest myFunctionalTest;

        public Form2(FunctionalTest myFunctionalTestHandle)
        {
            myFunctionalTest = myFunctionalTestHandle;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // Flashing Yes
        {
            myFunctionalTest.LEDsResult = true;
            //this.Dispose();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) // Flashing No
        {
            myFunctionalTest.LEDsResult = false;
            //this.Dispose();
            this.Close();
        }

    }
}
