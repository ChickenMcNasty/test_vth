﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VTH
{
    public partial class Form6 : Form
    {
        FunctionalTest myFunctionalTest;

        public Form6(FunctionalTest myFunctionalTestHandle)
        {
            myFunctionalTest = myFunctionalTestHandle;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // quit
        {
            myFunctionalTest.configListening = false;
            myFunctionalTest.configButtonResult = false;
            //this.Dispose();
            this.Close();
        }

        public void StatusPass()
        {
            this.label2.BackColor = System.Drawing.Color.Lime;
            this.label2.Text = "PASS";
            this.label2.Refresh();
        }
    }
}
