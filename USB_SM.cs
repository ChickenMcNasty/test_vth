﻿using System;
using Ivi.Visa.Interop;
using MainForm;

namespace AgilentInstruments
{
    class USB_SM
    {
        Form1 myForm;
        ResourceManager myRM;
        FormattedIO488Class mySMConnection;
        string myInstrumentLetter;
        
        public USB_SM(Form1 myFormHandle, ResourceManager myRMHandle, string instrumentLetter) //Constructor 
        
        {
            myForm = myFormHandle;
            myRM = myRMHandle;
            myInstrumentLetter = instrumentLetter;
        }

        public bool Connect()
        {
            try
               
            {
                mySMConnection = new FormattedIO488Class();
                mySMConnection.IO = (IMessage)myRM.Open("USB_SM_" + myInstrumentLetter, AccessMode.NO_LOCK, 2000, "");  //use USB_SM_(A or B) VISA Alias
                return true;
            }
            catch
            {
                myForm.StatusError("Unable to connect to Switch Matrix A!" + Environment.NewLine + "Ensure it is on, connected, and aliased as USB_SM_A.");
                return false;
            }
        }

        public bool Initialize()
            {
            int row;
            int column;

            for (row = 4; row > 0; row--)
            {
                for (column = 1; column < 9; column++)
                {
                    mySMConnection.WriteString("ROUTe:OPEN  (@" + row.ToString() + "0" + column.ToString() + ")", true);
                }
            }

            for (row = 4; row > 0; row--)
            {
                for (column = 1; column < 9; column++)
                {
                    mySMConnection.WriteString("ROUTe:OPEN?  (@" + row.ToString() + "0" + column.ToString() + ")", true);
                    if (int.Parse(mySMConnection.ReadString()) == 0)
                    {
                        myForm.StatusError("Relay Row:" + row.ToString() + " Column:0" + column.ToString() + " on USB_SM_" + myInstrumentLetter + " Not Opening!");
                        return false;
                    }
                }
            }
            return true;
        }

        public void CloseRelay(int row, int column)
        {
            mySMConnection.WriteString("ROUTe:CLOSe  (@" + row.ToString() + column.ToString("D2") + ")", true);
            mySMConnection.WriteString("ROUTe:CLOSe?  (@" + row.ToString() + column.ToString("D2") + ")", true);
            if (int.Parse(mySMConnection.ReadString()) == 0) myForm.StatusError("Relay Row:" + row.ToString() + " Column: " + column.ToString("D2") + " on USB_SM_" + myInstrumentLetter + " Not Closing!");
        }

        public void OpenRelay(int row, int column)
        {
            mySMConnection.WriteString("ROUTe:OPEN  (@" + row.ToString() + column.ToString("D2") + ")", true);
            mySMConnection.WriteString("ROUTe:OPEN?  (@" + row.ToString() + column.ToString("D2") + ")", true);
            if (int.Parse(mySMConnection.ReadString()) == 0) myForm.StatusError("Relay Row:" + row.ToString() + " Column: " + column.ToString("D2") + " on USB_SM_" + myInstrumentLetter + " Not Opening!");
        }

        public void Close()
        {
            mySMConnection.IO.Close();
        }
    }
}
