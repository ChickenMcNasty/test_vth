﻿using System;
using Ivi.Visa.Interop;
using MainForm;

namespace AgilentInstruments
{
    class USB_SMU
    {
        Form1 myForm;
        ResourceManager myRM;
        FormattedIO488Class mySMUConnection;
        
        const string NPLC = "10";        //Number of Power Line Cycles for measurements (lower is faster)

        public USB_SMU(Form1 myFormHandle, ResourceManager myRMHandle) //Constructor 
        {
            myForm = myFormHandle;
            myRM = myRMHandle;
        }
        
        public bool Connect()
        {
            try
            {
                mySMUConnection = new FormattedIO488Class();
                mySMUConnection.IO = (IMessage)myRM.Open("USB_SMU", AccessMode.NO_LOCK, 2000, "");  //use USB_SMU VISA Alias
                return true;
            }
            catch
            {
                myForm.StatusError("Unable to connect to SMU!" + Environment.NewLine + "Ensure it is on, connected, and aliased as USB_SMU.");
                return false;
            }
        }
        
        public bool Initialize()
        {
            //Initial Instrument Settings
            mySMUConnection.WriteString("SYSTem:LFRequency F60Hz", true);
            mySMUConnection.WriteString("SENSe:VOLTage:NPLC " + NPLC + ", (@1)", true);
            mySMUConnection.WriteString("SENSe:VOLTage:NPLC " + NPLC + ", (@2)", true);
            mySMUConnection.WriteString("SENSe:VOLTage:NPLC " + NPLC + ", (@3)", true);
            mySMUConnection.WriteString("SENSe:CURRent:NPLC " + NPLC + ", (@1)", true);
            mySMUConnection.WriteString("SENSe:CURRent:NPLC " + NPLC + ", (@2)", true);
            mySMUConnection.WriteString("SENSe:CURRent:NPLC " + NPLC + ", (@3)", true);
            mySMUConnection.WriteString("OUTPut OFF, (@1)", true);
            mySMUConnection.WriteString("OUTPut OFF, (@2)", true);
            mySMUConnection.WriteString("OUTPut OFF, (@3)", true);
            mySMUConnection.WriteString("SOURce:VOLTage:RANGe R20V, (@1)", true);
            mySMUConnection.WriteString("SOURce:VOLTage:RANGe R20V, (@2)", true);
            mySMUConnection.WriteString("SOURce:VOLTage:RANGe R20V, (@3)", true);
            mySMUConnection.WriteString("SOURce:CURRent:RANGe R120mA, (@1)", true);
            mySMUConnection.WriteString("SOURce:CURRent:RANGe R120mA, (@2)", true);
            mySMUConnection.WriteString("SOURce:CURRent:RANGe R120mA, (@3)", true);
            mySMUConnection.WriteString("SOURce:VOLTage:LIMit 20V, (@1)", true);
            mySMUConnection.WriteString("SOURce:VOLTage:LIMit 20V, (@2)", true);
            mySMUConnection.WriteString("SOURce:VOLTage:LIMit 20V, (@3)", true);
            mySMUConnection.WriteString("SOURce:CURRent:LIMit 120mA, (@1)", true);
            mySMUConnection.WriteString("SOURce:CURRent:LIMit 120mA, (@1)", true);
            mySMUConnection.WriteString("SOURce:CURRent:LIMit 120mA, (@1)", true);
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@1)", true);
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@2)", true);
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@3)", true);

            return true;
        }

        public void Off()
        {
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@1)", true);
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@2)", true);
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@3)", true);
            mySMUConnection.WriteString("OUTPut OFF, (@1)", true);
            mySMUConnection.WriteString("OUTPut OFF, (@2)", true);
            mySMUConnection.WriteString("OUTPut OFF, (@3)", true);
        }

        public void Off(int channel)
        {
            mySMUConnection.WriteString("SOURce:VOLTage 0, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("OUTPut OFF, (@" + channel.ToString() + ")", true);
        }

        public double GetAmps(int channel)
        {
            mySMUConnection.WriteString("MEASure:CURRent? (@" + channel.ToString() + ")", true);
            return double.Parse(mySMUConnection.ReadString());
        }

        public double GetVolts(int channel)
        {
            mySMUConnection.WriteString("MEASure:VOLTage? (@" + channel.ToString() + ")", true);
            return double.Parse(mySMUConnection.ReadString());
        }

        public void SetVoltage(int channel, double voltage)
        {
            double voltageRead;
            double voltageNew = voltage;
           
            mySMUConnection.WriteString("SOURce:CURRent:LIMit 120mA, (@" + channel.ToString() + ")", true);  // max out current limit
            mySMUConnection.WriteString("SOURce:VOLTage " + voltage.ToString("G4") + "V, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("OUTPut ON, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("MEASure:VOLTage? (@" + channel.ToString() + ")", true);
            voltageRead = double.Parse(mySMUConnection.ReadString());
            //Try to get it right by adjusting by the difference between measured and setpoint
            //Aiming for 0.05% accuracy and try a max of 10 times 
            int i = 0;
            while ((i < 10) && ((voltageRead < (voltage - (voltage * 0.0005))) || ((voltageRead > (voltage + (voltage * 0.0005))))))
            {
                voltageNew = voltageNew + ((voltage - voltageRead));
                //myForm.WriteSerialLog("Channel " + channel.ToString() + " set to " + voltageNew.ToString() + " volts!" + Environment.NewLine);
                mySMUConnection.WriteString("SOURce:VOLTage " + voltageNew.ToString("G4") + "V, (@" + channel.ToString() + ")", true);
                mySMUConnection.WriteString("MEAS:VOLT? (@" + channel.ToString() + ")", true);
                voltageRead = double.Parse(mySMUConnection.ReadString());
                i++;
            }
        }

        public void SetVoltage(int channel, double voltage, double currentLimit)
        {
            double voltageRead;
            double voltageNew = voltage;

            if (currentLimit > 0.01) mySMUConnection.WriteString("SOURce:CURRent:RANGe R120mA, (@" + channel.ToString() + ")", true);
            else if (currentLimit > 0.001) mySMUConnection.WriteString("SOURce:CURRent:RANGe R10mA, (@" + channel.ToString() + ")", true);
            else if (currentLimit > 0.0001) mySMUConnection.WriteString("SOURce:CURRent:RANGe R1mA, (@" + channel.ToString() + ")", true);
            else if (currentLimit > 0.00001) mySMUConnection.WriteString("SOURce:CURRent:RANGe R100uA, (@" + channel.ToString() + ")", true);
            else if (currentLimit > 0.000001) mySMUConnection.WriteString("SOURce:CURRent:RANGe R10uA, (@" + channel.ToString() + ")", true);
            else mySMUConnection.WriteString("SOURce:CURRent:RANGe R1uA, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("SOURce:CURRent:LIMit " + currentLimit.ToString("G4") + "A, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("SOURce:VOLTage " + voltage.ToString("G4") + "V, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("OUTPut ON, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("MEASure:VOLTage? (@" + channel.ToString() + ")", true);
            voltageRead = double.Parse(mySMUConnection.ReadString());
            //Try to get it right by adjusting by the difference between measured and setpoint
            //Aiming for 0.05% accuracy and try a max of 10 times 
            int i = 0;
            while ((i < 10) && ((voltageRead < (voltage - (voltage * 0.0005))) || ((voltageRead > (voltage + (voltage * 0.0005))))))
            {
                voltageNew = voltageNew + ((voltage - voltageRead));
                mySMUConnection.WriteString("SOURce:VOLTage " + voltageNew.ToString("G4") + "V, (@" + channel.ToString() + ")", true);
                mySMUConnection.WriteString("MEAS:VOLT? (@" + channel.ToString() + ")", true);
                voltageRead = double.Parse(mySMUConnection.ReadString());
                i++;
            }
        }

        public void SetCurrent(int channel, double amps)
        {
            double ampsRead;
            double ampsNew = amps;
            
            mySMUConnection.WriteString("SOURce:VOLTage:LIMit 20V, (@" + channel.ToString() + ")", true); // max out voltage limit
            mySMUConnection.WriteString("SOURce:CURRent " + amps.ToString("G4") + "A, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("OUTPut ON, (@" + channel.ToString() + ")", true);
            mySMUConnection.WriteString("MEASure:CURRent? (@" + channel.ToString() + ")", true);
            ampsRead = double.Parse(mySMUConnection.ReadString());
            //Try to get it right by adjusting by the difference between measured and setpoint
            //Aiming for 0.02% accuracy and try a max of 10 times 
            int i = 0;
            while ((i < 10) && ((ampsRead < (amps - (amps * 0.0002))) || ((ampsRead > (amps + (amps * 0.0002))))))
            {
                ampsNew = ampsNew + ((amps - ampsRead));
                mySMUConnection.WriteString("SOURce:CURRent " + ampsNew.ToString("G4") + "A, (@" + channel.ToString() + ")", true);
                mySMUConnection.WriteString("MEASure:CURRent? (@" + channel.ToString() + ")", true);
                ampsRead = double.Parse(mySMUConnection.ReadString());
                i++;
            }
        }

        public void Close()
        {
            mySMUConnection.IO.Close();
        }
    }
}
