/******** Evrisko Systems, LLC **************/
/****** VTH Functional Test Class ***********/
/*                                          */
/*                                          */
/********************************************/
/********************************************/

//Note: test can be sped up by using custom range and precision values for measurement SCPI commands instead of AUTO and MAX

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Net;
using System.Globalization;
using Ivi.Visa.Interop;
using AgilentInstruments;
using MainForm;
using System.Runtime.InteropServices;

namespace VTH
{
    public class FunctionalTest
    {
        //[DllImport("user32.dll")]
        //public static extern int SetForegroundWindow(IntPtr hWnd);
        
        /**************CONSTANTS***************/
        //Switch Matrix Wiring
        const int R_GND = 4;      //Row 4
        const int R_SMU1_OP = 3;  //Row 3
        const int R_DMM_VP = 2;   //Row 2
        const int C_BATT = 1;     //Column 1
        const int C_VDD_5V = 2;   //Column 2
        const int C_VDD_3V3 = 3;  //Column 3
        const int C_VDD_1V8 = 4;  //Column 4
        const int C_ALARM1 = 5;   //Column 5
        const int C_ALARM2 = 6;   //Column 6
        const int C_DC_CRTL = 7;  //Column 7
        const int C_PGRM_EN = 8;  //Column 8
        //Battery Voltages to Test
        const double IDEALBATTERY = 3.000; //Nominal Battery Voltage for two AAs
        const double MAXBATTERY = 3.200;   //Max tested Battery Voltage
        const double MINBATTERY = 2.200;   //Min tested Battery Voltage
        const double RUNBATTERY = 4.000;   //This is higher than batteries so the SMU can handle the load
        //Tolerance for Regulators (0.01 equals 1%)
        const double TOLERANCE_5V = 0.02;
        const double TOLERANCE_3V3 = 0.02;
        const double TOLERANCE_1V8 = 0.02;
        //Tolerance for sensor measured battery voltage
        const double TOLERANCE_WIFIBATTERY = 0.02;
        //Timeout for firmware flashing and getNode packet retrieval (seconds)
        const int MACTIME = 10;
        const int FLASHTIME = 45;
        const int LISTENTIME = 120;
        //Network setup parameters
        const string SSID = "WiFiSensor";
        const string PASSPHRASE = "WiFiSensor";
        const string CHANNEL = "6";
        const byte MODULETYPE = 0x07;
        //const byte MODULESUBTYPE = 0x03;
		const byte GSMODEL = 0x01; //1011
		const byte GSCOMPAT = 0x00;
        

        
        /************Global Variables***********/
        Form1 myForm; //Handle to the calling object 
        Form2 myForm2;
        Form4 myForm4;
        Form5 myForm5;
        Form6 myForm6;
        Form7 myForm7;
        ResourceManager myRM;   //The resource manager interface for connection to VISA/IVI drivers
        USB_SMU myUSB_SMU;
        USB_SM myUSB_SM_A;
        USB_SM myUSB_SM_B;
        USB_DMM myUSB_DMM;
        Process getNode;
        Process gsflash;
        SerialPort myCOM_PORT;
        public static bool busy;
        public bool setupListening, setupButtonResult, configListening, configButtonResult, resetListening, resetButtonResult, LEDsResult, jumperResult;
        private int subType = 0; // 1 is temp only, 2 is humidity only, 3 is both temp+humidity
        private string getNodePacket = null;
        private byte[] MACAddress = {00,00,00,00,00,00};
        private string GSFirmwareVersion = "0.00";
        private string EvriskoFirmwareVersion = "0.00";
        private string serialResponse = "";

        Thread SMUInitThread;
        Thread SM_AInitThread;
        Thread SM_BInitThread;
        Thread DMMInitThread;
        Thread ListenForSetupButtonThread;
        Thread ListenForConfigButtonThread;
        Thread ListenForResetButtonThread;
        Thread GsflashTimeoutThread;
        Thread GetNodeTimeoutThread;
        
        public FunctionalTest(Form1 myFormHandle) //Constructor for a new functional test object
        {
            myForm = myFormHandle;
        }

        public void StartTest()
        {
            FunctionalTest.busy = true;
            bool initializeResult = true, testResult = true;

            myForm.DisableStartButton();
            //string serNum = myForm.GetSerNum();
            myForm.SetProgressBar(0);
            myForm.StatusTesting();

            subType = myForm.GetSubType();
            if (subType == 0x00)
            {
                testResult = false;
                myForm.StatusError("You must select a subtype!");
            }

            //myForm.WriteSerialLog("   *) Using '" + SSID + "' wireless network on channel " + CHANNEL + Environment.NewLine);
            try
            {
                MACAddress = myForm.GetMAC();
            }
            catch
            {
                testResult = false;
                myForm.StatusError("Enter a Valid MAC Address with no spaces!");
            }
            string MACString = "";
            for (int x = 0; x < 6; x++)
            {
                MACString += Convert.ToString(MACAddress[x], 16).PadLeft(2, '0');
                if (x < 5) MACString += "-";
            }
            if (((MACAddress[3] == 0) && (MACAddress[4] == 0) && (MACAddress[5] == 0)) || ((MACAddress[3] == 0xFF) && (MACAddress[4] == 0xFF) && (MACAddress[5] == 0xFF)))
            {
                testResult = false;
                myForm.StatusError("MAC Address cannot be xx-xx-xx-00-00-00 or xx-xx-xx-FF-FF-FF!");
            }
            else
            {

            }
            myForm.ReformatMAC();

            myForm.WriteTestLog(DateTime.Now + " - Starting test for MAC address: " + MACString);
            myForm.WriteSerialLog(DateTime.Now + " - Starting test for MAC address: " + MACString);

            if (subType == 0x01) myForm.WriteTestLog(" (Temperature Only)" + Environment.NewLine);
            else if (subType == 0x02) myForm.WriteTestLog(" (Humidity Only)" + Environment.NewLine);
            else if (subType == 0x03) myForm.WriteTestLog(" (Temp & Humidity)" + Environment.NewLine);
            else myForm.WriteTestLog(" (Unknown)" + Environment.NewLine);
            if (subType == 0x01) myForm.WriteSerialLog(" (Temperature Only)" + Environment.NewLine);
            else if (subType == 0x02) myForm.WriteSerialLog(" (Humidity Only)" + Environment.NewLine);
            else if (subType == 0x03) myForm.WriteSerialLog(" (Temp & Humidity)" + Environment.NewLine);
            else myForm.WriteSerialLog(" (Unknown)" + Environment.NewLine);
            
            //Initialization
            myForm.WriteTestLog("   *) Initializing Instruments\t\t\t\t\t\t\t\t\t");
            initializeResult = this.InitializeInstruments();
            if (!initializeResult)
            {
                testResult = false;
                myForm.WriteTestLog("ERROR" + Environment.NewLine + "   Aborting Test..." + Environment.NewLine);
            }
            else myForm.WriteTestLog("DONE" + Environment.NewLine);
            myForm.SetProgressBar(2);
            
            int stepNumber = 1;
            //Test Steps
            if (testResult) testResult = TestRange(StepIDResistor, stepNumber++, "Test Fixture ID Resistor\t\t\t", 100e3, 140e3, "Ω", "Check for incorrect test fixture/wiring");
            myForm.SetProgressBar(3);
            if (testResult) testResult = TestPassFail(StepJumper, stepNumber++, "Check Position of Jumper\t\t\t", "Install programming jumper in RUN position (pins 2 & 3)");
            myForm.SetProgressBar(4);           
            if (testResult) testResult = TestRange(StepResistanceBATT, stepNumber++, "BATT to GND Resistance\t\t\t", 1e6, 1e50, "Ω", "Check for a short circuit or open between BATT and GND");
            myForm.SetProgressBar(6);
            if (testResult) testResult = TestRange(StepResistanceVDD_5V, stepNumber++, "VDD_5V to GND Resistance\t\t\t", 1e6, 1e50, "Ω", "Check for a short circuit or open between VDD_5V and GND");
            myForm.SetProgressBar(7);
            if (testResult) testResult = TestRange(StepResistanceVDD_3V3, stepNumber++, "VDD_3V3 to GND Resistance\t\t\t", 1e6, 1e50, "Ω", "Check for a short circuit or open between VDD_3V3 and GND");
            myForm.SetProgressBar(8);
            if (testResult) testResult = TestRange(StepResistanceVDD_1V8, stepNumber++, "VDD_1V8 to GND Resistance\t\t\t", 1e6, 1e50, "Ω", "Check for a short circuit or open between VDD_1V8 and GND");
            myForm.SetProgressBar(9);
            if (testResult) testResult = TestRange(StepIdealBatteryCurrent, stepNumber++, "Apply " + IDEALBATTERY.ToString("F2") + "V and Measure Current\t\t", 5e-3, 50e-3, "A", "Check for excessive current in one of the VDD lines");
            myForm.SetProgressBar(10);
            if (subType == 1) //1 is temp only with no 5V regulator present
            {
                if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage with " + IDEALBATTERY.ToString("F2") + "V Battery\t\t", -0.01, 2, "V", "Check Subtype and unpopulated 5V regulator area");
            }
            else
            {
                if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage with " + IDEALBATTERY.ToString("F2") + "V Battery\t\t", 5 - 5 * TOLERANCE_5V, 5 + 5 * TOLERANCE_5V, "V", "Check subtype and all components on 5V regulator");
            }
            myForm.SetProgressBar(11);
            if (testResult) testResult = TestRange(StepVoltageVDD_3V3, stepNumber++, "VDD_3V3 Voltage with " + IDEALBATTERY.ToString("F2") + "V Battery\t", 3.3 - 3.3 * TOLERANCE_3V3, 3.3 + 3.3 * TOLERANCE_3V3, "V", "Check all components on 3.3V regulator");
            myForm.SetProgressBar(12);
            if (testResult) testResult = TestRange(StepVoltageVDD_1V8, stepNumber++, "VDD_1V8 Voltage with " + IDEALBATTERY.ToString("F2") + "V Battery\t", 1.8 - 1.8 * TOLERANCE_1V8, 1.8 + 1.8 * TOLERANCE_1V8, "V", "Check Gainspan's internal 1.8V regulator");
            myForm.SetProgressBar(13);
            if (testResult) testResult = TestRange(StepMaxBatteryCurrent, stepNumber++, "Apply " + MAXBATTERY.ToString("F2") + "V and Measure Current\t\t", 5e-3, 50e-3, "A", "Check for excessive current in one of the VDD lines");
            myForm.SetProgressBar(14);
            if (subType == 1)
            {
                if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage with " + MAXBATTERY.ToString("F2") + "V Battery\t", -0.01, 2, "V", "Check Subtype and unpopulated 5V regulator area");
            }
            else
            {
                if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage with " + MAXBATTERY.ToString("F2") + "V Battery\t", 5 - 5 * TOLERANCE_5V, 5 + 5 * TOLERANCE_5V, "V", "Check subtype and all components on 5V regulator");
            }
            myForm.SetProgressBar(15);
            if (testResult) testResult = TestRange(StepVoltageVDD_3V3, stepNumber++, "VDD_3V3 Voltage with " + MAXBATTERY.ToString("F2") + "V Battery\t", 3.3 - 3.3 * TOLERANCE_3V3, 3.3 + 3.3 * TOLERANCE_3V3, "V", "Check all components on 3.3V regulator");
            myForm.SetProgressBar(16);
            if (testResult) testResult = TestRange(StepVoltageVDD_1V8, stepNumber++, "VDD_1V8 Voltage with " + MAXBATTERY.ToString("F2") + "V Battery\t", 1.8 - 1.8 * TOLERANCE_1V8, 1.8 + 1.8 * TOLERANCE_1V8, "V", "Check Gainspan's internal 1.8V regulator");
            myForm.SetProgressBar(17);
            if (testResult) testResult = TestRange(StepMinBatteryCurrent, stepNumber++, "Apply " + MINBATTERY.ToString("F2") + "V and Measure Current\t\t", 5e-3, 50e-3, "A", "Check for excessive current in one of the VDD lines");
            myForm.SetProgressBar(18);
            if (subType == 1)
            {
                if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage with " + MINBATTERY.ToString("F2") + "V Battery\t", -0.01, 2, "V", "Check Subtype and unpopulated 5V regulator area");
            }
            else
            {
                if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage with " + MINBATTERY.ToString("F2") + "V Battery\t", 5 - 5 * TOLERANCE_5V, 5 + 5 * TOLERANCE_5V, "V", "Check subtype and all components on 5V regulator");
            }
            myForm.SetProgressBar(19);
            if (testResult) testResult = TestRange(StepVoltageVDD_3V3, stepNumber++, "VDD_3V3 Voltage with " + MINBATTERY.ToString("F2") + "V Battery\t", 3.3 - 3.3 * TOLERANCE_3V3, 3.3 + 3.3 * TOLERANCE_3V3, "V", "Check all components on 3.3V regulator");
            myForm.SetProgressBar(20);
            if (testResult) testResult = TestRange(StepVoltageVDD_1V8, stepNumber++, "VDD_1V8 Voltage with " + MINBATTERY.ToString("F2") + "V Battery\t", 1.8 - 1.8 * TOLERANCE_1V8, 1.8 + 1.8 * TOLERANCE_1V8, "V", "Check Gainspan's internal 1.8V regulator");
            myForm.SetProgressBar(21);
            if (testResult) testResult = TestRange(StepShutdownCurrent, stepNumber++, "Apply " + IDEALBATTERY.ToString("F2") + "V, Shutdown, and Meas Current\t", 0, 10e-3, "A", "Check for excessive current in one of the VDD lines");
            myForm.SetProgressBar(22);
            if (testResult) testResult = TestRange(StepVoltageVDD_5V, stepNumber++, "VDD_5V Voltage During Shutdown\t\t", -0.01, 2, "V", "Check all components on 5V regulator");
            myForm.SetProgressBar(23);
            if (testResult) testResult = TestRange(StepVoltageVDD_3V3, stepNumber++, "VDD_3V3 Voltage During Shutdown\t\t", -0.01, 1, "V", "Check all components on 3.3V regulator");
            myForm.SetProgressBar(24);
            if (testResult) testResult = TestRange(StepVoltageVDD_1V8, stepNumber++, "VDD_1V8 Voltage During Shutdown\t\t", -0.01, 1, "V", "Check Gainspan's internal 1.8V regulator");
            myForm.SetProgressBar(25);
            if (testResult) testResult = TestRange(StepVDD_5VRipple, stepNumber++, "RMS Ripple Voltage on VDD_5V\t\t", 0, 0.1, "V", "Check for faulty 5V regulator or missing/faulty capacitors");
            myForm.SetProgressBar(26);
            if (testResult) testResult = TestRange(StepVDD_3V3Ripple, stepNumber++, "RMS Ripple Voltage on VDD_3V3\t\t", 0, 0.1, "V", "Check for faulty 3.3V regulator or missing/faulty capacitors");
            myForm.SetProgressBar(27);
            if (testResult) testResult = TestRange(StepVDD_1V8Ripple, stepNumber++, "RMS Ripple Voltage on VDD_1V8\t\t", 0, 0.1, "V", "Check for faulty Gainspan module or missing/faulty capacitors");
            myForm.SetProgressBar(28);
            if (testResult) testResult = TestPassFail(StepSetupButton, stepNumber++, "Verifying Setup Button\t\t\t", "Check for a bad switch at SW2");
            myForm.SetProgressBar(29);
            if (testResult) testResult = TestPassFail(StepConfigButton, stepNumber++, "Verifying Config Button\t\t\t", "Check for a bad switch at SW4");
            myForm.SetProgressBar(30);
            if (testResult) testResult = TestPassFail(StepResetButton, stepNumber++, "Verifying Reset Button\t\t\t", "Check for a bad switch at SW3");
            myForm.SetProgressBar(31);
            //if (testResult) testResult = TestPassFail(StepGetMAC, stepNumber++, "Getting existing MAC Address\t\t", "Check that file is present and serial connector is connected");
            //myForm.SetProgressBar(32);
            if (testResult) testResult = TestPassFail(StepFlashProgram, stepNumber++, "Flashing burn file to WiFi Module\t", "Check that file is present and serial connector is connected");
            myForm.SetProgressBar(83);
            if (testResult) testResult = TestPassFail(StepSetMAC, stepNumber++, "Setting MAC Address\t\t\t", "Check that file is present and serial connector is connected");
            myForm.SetProgressBar(84);
            if (testResult) testResult = TestPassFail(StepSaveConfig, stepNumber++, "Saving config data in flash\t\t", "Check for faulty serial connection or bad flash memory");
            myForm.SetProgressBar(85);
            if (testResult) testResult = TestPassFail(StepCheckSensors, stepNumber++, "Verify Sensors Installed\t\t\t", "Verify the Subtype selection, burn file, and check temperature and humidity sensors");
            myForm.SetProgressBar(86);
            if (testResult) testResult = TestPassFail(StepSetupNode, stepNumber++, "Setup Node via Serial Port\t\t", "Check for faulty serial connection or corrupted burn file");
            myForm.SetProgressBar(87);
            if (testResult) testResult = TestPassFail(StepLEDs, stepNumber++, "Check Operation of LEDs\t\t\t", "Check for correct LED polarity and operation");
            myForm.SetProgressBar(88);
            if (testResult) testResult = TestPassFail(StepRetrievePacket, stepNumber++, "Retrieve Packet via WiFi\t\t\t", "Check for bad antenna wire or bad network connection/address");
            myForm.SetProgressBar(90);
            if (testResult) testResult = TestRange(StepParseVoltage, stepNumber++, "Parse Battery Voltage from Packet\t", 2.5, 6, "V", "Check for bad battery connection");
            myForm.SetProgressBar(92);
            if (testResult) testResult = TestRange(StepParseRSSI, stepNumber++, "Parse Signal Strength from Packet\t", -50, 0, "dBm", "Check for bad antenna wire, low signal strength");
            myForm.SetProgressBar(93);
            if (subType != 2) if (testResult) testResult = TestRange(StepParseTemperature, stepNumber++, "Parse Temperature from Packet\t\t", 10, 35, "°C", "Check for bad temp probe");
            myForm.SetProgressBar(94);
            if (subType != 1) if (testResult) testResult = TestRange(StepParseHumidity, stepNumber++, "Parse Humidity from Packet\t\t", 5, 95, "%", "Check for bad or missing humidity sensor");
            myForm.SetProgressBar(95);
            if (testResult) testResult = TestPassFail(StepAlarmPins, stepNumber++, "Check Both Alarm Inputs\t\t\t", "Check for faulty serial connection or Gainspan Module");
            myForm.SetProgressBar(98);

            //End of test
            if (testResult)
            {
                myForm.StatusPass();               
                myForm.WriteTestLog("   *) Module Subtype \t\t\t\t\t\t\t" + subType.ToString());
                if (subType == 1) myForm.WriteTestLog(" (Temperature Only)" + Environment.NewLine);
                else if (subType == 2) myForm.WriteTestLog(" (Humidity Only)" + Environment.NewLine);
                else if (subType == 3) myForm.WriteTestLog(" (Temp & Humidity)" + Environment.NewLine);
                else myForm.WriteSerialLog(" (Unknown)" + Environment.NewLine);
                myForm.WriteTestLog("   *) WLAN Firmware Version \t\t\t\t\t\tv" + GSFirmwareVersion + Environment.NewLine);
                myForm.WriteTestLog("   *) Application Firmware Version \t\t\t\t\tv" + EvriskoFirmwareVersion + Environment.NewLine);
                myForm.WriteTestLog("   *) Automated Test Program Version \t\t\t\t\tv" + myForm.TestVersion + Environment.NewLine);
                myForm.WriteSerialLog(DateTime.Now + " - Test complete (PASS) for MAC address: " + MACString + Environment.NewLine);
                myForm.WriteTestLog(DateTime.Now + " - Test complete (PASS) for MAC address: " + MACString + Environment.NewLine);
                myForm.PrintIfNecessary();
                myForm.IncrementMAC();
            }
            else if (initializeResult)
            {
                myForm.StatusFail();
                //open up all relays and turn all outputs off
                bool initSMU = true, initSM_A = true, initSM_B = true, initDMM = true;
                //Setup the init threads to run in parallel via delegate functions (makes init way faster)
                try { SMUInitThread.Join(); } catch {}
                try { SM_AInitThread.Join(); } catch {}
                try { SM_BInitThread.Join(); } catch {}
                try { DMMInitThread.Join(); } catch {}

                SMUInitThread = new Thread(delegate() { initSMU = myUSB_SMU.Initialize(); });
                SM_AInitThread = new Thread(delegate() { initSM_A = myUSB_SM_A.Initialize(); });
                SM_BInitThread = new Thread(delegate() { initSM_B = myUSB_SM_B.Initialize(); });
                DMMInitThread = new Thread(delegate() { initDMM = myUSB_DMM.Initialize(); });
                SM_AInitThread.Start();
                SM_BInitThread.Start();
                SMUInitThread.Start();
                DMMInitThread.Start();
                //Wait for all threads to complete
                DMMInitThread.Join();
                SMUInitThread.Join();
                SM_AInitThread.Join();
                SM_BInitThread.Join();
                myForm.WriteSerialLog(DateTime.Now + " - Test complete (FAIL) for MAC address: " + MACString + Environment.NewLine);
                myForm.WriteTestLog(DateTime.Now + " - Test complete (FAIL) for MAC address: " + MACString + Environment.NewLine);
            }
            myForm.WriteSerialLog("" + Environment.NewLine); //Blank line on serial log
            myForm.WriteTestLog("" + Environment.NewLine);  //Blank line on test log
            //If the instruments were connected at the start of the test
            if (initializeResult) this.CloseInstruments();
            myForm.SetProgressBar(100);
            myForm.WriteTestLogFileToDisk();
            myForm.WriteSerialLogFileToDisk();

            myForm.EnableStartButton();

            //myForm.Show();
            myForm.ReturnFocus();
            SendKeys.SendWait("{TAB}");
            SendKeys.SendWait("+{TAB}");  //This is a hack to get focus back in the MAC Address textbox
            FunctionalTest.busy = false;
        }

        private bool TestRange(Func<double> myMethodName, int testStepNumber, string testName, double minimum, double maximum, string unit, string troubleMessage)
        {
            string resultString;

            myForm.WriteTestLog("   " + testStepNumber + ") " + testName + "Expect: " + ToEngineeringNotation(minimum, "G3") + unit + " to " + ToEngineeringNotation(maximum, "G3") + unit + "\t");
            double result = myMethodName();
            resultString = ToEngineeringNotation(result, "G4");
            if (resultString.Length >= 7) myForm.WriteTestLog("Actual: " + resultString + unit + "\t");
            else if (resultString.Length == 6) myForm.WriteTestLog("Actual:  " + resultString + unit + "\t");
            else if (resultString.Length == 5) myForm.WriteTestLog("Actual:   " + resultString + unit + "\t");
            else if (resultString.Length == 4) myForm.WriteTestLog("Actual:    " + resultString + unit + "\t");
            else if (resultString.Length == 3) myForm.WriteTestLog("Actual:     " + resultString + unit + "\t");
            else if (resultString.Length == 2) myForm.WriteTestLog("Actual:      " + resultString + unit + "\t");
            else if (resultString.Length == 1) myForm.WriteTestLog("Actual:       " + resultString + unit + "\t");
            else myForm.WriteTestLog("Actual:        " + resultString + unit + "\t");
            if ((result >= minimum) && (result <= maximum))
            {
                myForm.WriteTestLog("PASS" + Environment.NewLine);
                return true; 
            }
            else
            {
                myForm.WriteTestLog("FAIL!" + Environment.NewLine + "   Aborting Test...." + Environment.NewLine);
                myForm.WriteTestLog("   ****" + troubleMessage + "****" + Environment.NewLine);
                return false;
            }
        }

        private bool TestPassFail(Func<bool> myMethodName, int testStepNumber, string testName, string troubleMessage)
        {
            myForm.WriteTestLog("   " + testStepNumber + ") " + testName + "Expect: (Pass/Fail)\t\t\t\t");
            bool result = myMethodName();
            if (!result)
            {
                myForm.WriteTestLog("FAIL!" + Environment.NewLine + "   Aborting Test..." + Environment.NewLine);
                myForm.WriteTestLog("   ****" + troubleMessage + "****" + Environment.NewLine);
                return false;
            }
            else
            {
                myForm.WriteTestLog("PASS" + Environment.NewLine);
                return true;
            }
        }

        private bool InitializeInstruments()  //Opens Connections to instruments and initializes them
        {
            bool initSMU = true, initSM_A = true, initSM_B = true, initDMM = true;
            //Connect to the IO Libraries       
            try
            {
                myRM = new ResourceManager();
            }
            catch
            {
                myForm.StatusError("Unable to connect to Resource Manager!" + Environment.NewLine + "Verify installation of Agilent I/O Libraries.");
                return false;
            }
            //Create the test instrument objects
            myUSB_SMU = new USB_SMU(myForm, myRM);
            myUSB_SM_A = new USB_SM(myForm, myRM, "A");
            myUSB_SM_B = new USB_SM(myForm, myRM, "B");
            myUSB_DMM = new USB_DMM(myForm, myRM);
            //Physically connect to them
            if (!myUSB_SMU.Connect()) return false;
            if (!myUSB_SM_A.Connect()) return false;
            if (!myUSB_SM_B.Connect()) return false;
            if (!myUSB_DMM.Connect()) return false;
            try { SMUInitThread.Join(); }
            catch { }
            try { SM_AInitThread.Join(); }
            catch { }
            try { SM_BInitThread.Join(); }
            catch { }
            try { DMMInitThread.Join(); }
            catch { }
            //Setup the init threads to run in parallel via delegate functions (makes init way faster)
            SMUInitThread = new Thread(delegate() { initSMU = myUSB_SMU.Initialize(); });
            SM_AInitThread = new Thread(delegate() { initSM_A = myUSB_SM_A.Initialize(); });
            SM_BInitThread = new Thread(delegate() { initSM_B = myUSB_SM_B.Initialize(); });
            DMMInitThread = new Thread(delegate() { initDMM = myUSB_DMM.Initialize(); });
            SM_AInitThread.Start();
            SM_BInitThread.Start();
            SMUInitThread.Start();
            DMMInitThread.Start();
            //Wait for all threads to complete
            DMMInitThread.Join();
            SMUInitThread.Join();
            SM_AInitThread.Join();
            SM_BInitThread.Join();
            //Return true if all inits were successful
            if (initSMU && initSM_A && initSM_B && initDMM) return true;
            else return false;
        }

        private void CloseInstruments()  //Closes the instruments
        {
            //Outputs off and DMM back to volts
            myUSB_DMM.ConfigForVolts();
            myUSB_SMU.Off();
            //Closing device connections.
            myUSB_SMU.Close();
            myUSB_SM_A.Close();
            myUSB_SM_B.Close();
            myUSB_DMM.Close();
        }

        private double StepIDResistor()
        {
            myUSB_SM_A.CloseRelay(1, 1);
            double resistance = myUSB_DMM.GetResistance(1000000); //1meg range
            myUSB_SM_A.OpenRelay(1, 1);
            return resistance;
        }

        private bool StepJumper()
        {

            myForm7 = new Form7(this);
            myForm7.ShowDialog();
            return jumperResult;
        }

        private double StepResistanceBATT()
        {
            double ohms;

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_BATT);
            ohms = myUSB_DMM.GetResistance(100);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_BATT);
            
            return ohms;
        }

        private double StepResistanceVDD_5V()
        {
            double ohms;

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_5V);
            ohms = myUSB_DMM.GetResistance(100);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_5V);

            return ohms;
        }

        private double StepResistanceVDD_3V3()
        {
            double ohms;

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_3V3);
            ohms = myUSB_DMM.GetResistance(100);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_3V3);

            return ohms;
        }

        private double StepResistanceVDD_1V8()
        {
            double ohms;

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_1V8);
            ohms = myUSB_DMM.GetResistance(100);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_1V8);
            myUSB_DMM.ConfigForVolts();

            return ohms;
        }

        private double StepIdealBatteryCurrent()
        {
            //Put gainspan into program mode
            myUSB_SM_A.CloseRelay(1, C_PGRM_EN);
            myUSB_SM_A.CloseRelay(1, C_VDD_3V3);
            
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            //Thread.Sleep(500);
            myUSB_SMU.SetVoltage(1, IDEALBATTERY);
            //myUSB_SMU.SetVoltage(2, IDEALBATTERY);
            //myUSB_SMU.SetVoltage(3, IDEALBATTERY);
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);           
            //Thread.Sleep(500);
            
            //myForm.WriteSerialLog("Channel 1 Amps: " + myUSB_SMU.GetAmps(1).ToString() + Environment.NewLine);
            //myForm.WriteSerialLog("Channel 2 Amps: " + myUSB_SMU.GetAmps(2).ToString() + Environment.NewLine);
            //myForm.WriteSerialLog("Channel 3 Amps: " + myUSB_SMU.GetAmps(3).ToString() + Environment.NewLine + Environment.NewLine);
            
            double amps = myUSB_SMU.GetAmps(1);// +myUSB_SMU.GetAmps(2) + myUSB_SMU.GetAmps(3);
            if (amps > 0) return amps;
            else return 1e-50;
        }

        private double StepMaxBatteryCurrent()
        {
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            //Thread.Sleep(500);
            myUSB_SMU.SetVoltage(1, MAXBATTERY);
            //myUSB_SMU.SetVoltage(2, MAXBATTERY);
            //myUSB_SMU.SetVoltage(3, MAXBATTERY);
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);
            //Thread.Sleep(500);

            //myForm.WriteSerialLog("Channel 1 Amps: " + myUSB_SMU.GetAmps(1).ToString() + Environment.NewLine);
            //myForm.WriteSerialLog("Channel 2 Amps: " + myUSB_SMU.GetAmps(2).ToString() + Environment.NewLine);
            //myForm.WriteSerialLog("Channel 3 Amps: " + myUSB_SMU.GetAmps(3).ToString() + Environment.NewLine + Environment.NewLine);

            double amps = myUSB_SMU.GetAmps(1);// +myUSB_SMU.GetAmps(2) + myUSB_SMU.GetAmps(3);
            if (amps > 0) return amps;
            else return 1e-50;
        }

        private double StepMinBatteryCurrent()
        {
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            //Thread.Sleep(500);
            myUSB_SMU.SetVoltage(1, MINBATTERY);
            //myUSB_SMU.SetVoltage(2, MINBATTERY);
            //myUSB_SMU.SetVoltage(3, MINBATTERY);
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);
            //Thread.Sleep(500);

            //myForm.WriteSerialLog("Channel 1 Amps: " + myUSB_SMU.GetAmps(1).ToString() + Environment.NewLine);
            //myForm.WriteSerialLog("Channel 2 Amps: " + myUSB_SMU.GetAmps(2).ToString() + Environment.NewLine);
            //myForm.WriteSerialLog("Channel 3 Amps: " + myUSB_SMU.GetAmps(3).ToString() + Environment.NewLine + Environment.NewLine);

            double amps = myUSB_SMU.GetAmps(1);// +myUSB_SMU.GetAmps(2) + myUSB_SMU.GetAmps(3);
            if (amps > 0) return amps;
            else return 1e-50;
        }

        private double StepShutdownCurrent()
        {
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SMU.SetVoltage(1, IDEALBATTERY);
            myUSB_SM_A.CloseRelay(R_GND, C_DC_CRTL);
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);
            
            Thread.Sleep(1000);
            
            double amps = myUSB_SMU.GetAmps(1);
            if (amps > 0) return amps;
            else return 1e-50;
        }

        private double StepVoltageVDD_5V()
        {
            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_5V);
            //Thread.Sleep(10);
            double volts = myUSB_DMM.GetVolts(10);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_5V);

            return volts;
        }

        private double StepVoltageVDD_3V3()
        {
            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_3V3);
            //Thread.Sleep(10);
            double volts = myUSB_DMM.GetVolts(10);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_3V3);

            return volts;
        }

        private double StepVoltageVDD_1V8()
        {
            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_1V8);
            //Thread.Sleep(10);
            double volts = myUSB_DMM.GetVolts(10);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_1V8);

            return volts;
        }

        private double StepVDD_5VRipple()
        {
            //Return battery to ideal voltage
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SM_A.OpenRelay(R_GND, C_DC_CRTL);
            myUSB_SMU.SetVoltage(1, IDEALBATTERY); //dual supplies to prepare for run mode
            myUSB_SMU.SetVoltage(2, IDEALBATTERY);
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_5V);
            double volts = myUSB_DMM.GetVoltsAC(10);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_5V);

            return volts;
        }

        private double StepVDD_3V3Ripple()
        {
            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_5V);
            double volts = myUSB_DMM.GetVoltsAC(10);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_5V);

            return volts;
        }

        private double StepVDD_1V8Ripple()
        {
            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_5V);
            double volts = myUSB_DMM.GetVoltsAC(10);
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_5V);

            return volts;
        }

        private bool StepSetupButton()
        {            
            setupListening = true;
            try { ListenForSetupButtonThread.Join(); }
            catch { }
            ListenForSetupButtonThread = new Thread(delegate() { setupButtonResult = ListenForSetupButton(); });
            ListenForSetupButtonThread.Start();
            myForm4 = new Form4(this);
            myForm4.ShowDialog();
            ListenForSetupButtonThread.Join();
            return setupButtonResult;
        }

        private bool ListenForSetupButton()
        {
            double volts;

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_ALARM1);
            while (this.setupListening)
            {
                volts = myUSB_DMM.GetVolts(10);
                if (volts > 2.4)
                {
                    while (this.setupListening)
                    {
                        volts = myUSB_DMM.GetVolts(10);
                        if (volts < 0.8)
                        {
                            myForm4.StatusPass();
                            while (myUSB_DMM.GetVolts(10) < 2.4) Thread.Sleep(10);
                            myUSB_SM_A.OpenRelay(R_DMM_VP, C_ALARM1);
                            //myForm4.Dispose();
                            myForm4.Close();
                            return true;
                        }
                    }
                }
            }
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_ALARM1);
            return false;
        }

        private bool StepConfigButton()
        {
            configListening = true;
            try { ListenForConfigButtonThread.Join(); }
            catch { }
            ListenForConfigButtonThread = new Thread(delegate() { configButtonResult = ListenForConfigButton(); });
            ListenForConfigButtonThread.Start();
            myForm5 = new Form5(this);
            myForm5.ShowDialog();
            ListenForConfigButtonThread.Join();
            return configButtonResult;
        }

        private bool ListenForConfigButton()
        {
            double volts;
            
            myUSB_SM_A.CloseRelay(R_DMM_VP, C_ALARM2);
            while (this.configListening)
            {
                volts = myUSB_DMM.GetVolts(10);
                if (volts > 2.4)
                {
                    while (this.configListening)
                    {
                        volts = myUSB_DMM.GetVolts(10);
                        if (volts < 0.8)
                        {
                            myForm5.StatusPass();
                            while (myUSB_DMM.GetVolts(10) < 2.4) Thread.Sleep(10);
                            myUSB_SM_A.OpenRelay(R_DMM_VP, C_ALARM2);
                            //myForm5.Dispose();
                            myForm5.Close();
                            return true;
                        }
                    }
                }
            }
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_ALARM2);
            return false;
        }

        private bool StepResetButton()
        {
            resetListening = true;
            try { ListenForSetupButtonThread.Join(); }
            catch { }
            ListenForResetButtonThread = new Thread(delegate() { resetButtonResult = ListenForResetButton(); });
            ListenForResetButtonThread.Start();
            myForm6 = new Form6(this);
            myForm6.ShowDialog();
            ListenForResetButtonThread.Join();
            return resetButtonResult;
        }

        private bool ListenForResetButton()
        {
            double volts;

            myUSB_SM_A.CloseRelay(R_DMM_VP, C_VDD_3V3);
            while (this.resetListening)
            {
                volts = myUSB_DMM.GetVolts(10);
                if (volts > 2.4)
                {
                    while (this.resetListening)
                    {
                        volts = myUSB_DMM.GetVolts(10);
                        if (volts < 0.8)
                        {
                            myForm6.StatusPass();
                            while (myUSB_DMM.GetVolts(10) < 2.4) Thread.Sleep(10);
                            myUSB_SM_A.OpenRelay(R_DMM_VP, C_VDD_3V3);
                            //myForm6.Dispose();
                            myForm6.Close();
                            return true;
                        }
                    }
                }
            }
            myUSB_SM_A.OpenRelay(R_DMM_VP, C_ALARM1);
            return false;
        }

        private bool StepGetMAC()
        {
            Process gsflashGetMAC = new Process();
            string comPort;

            gsflashGetMAC.StartInfo.UseShellExecute = false;
            gsflashGetMAC.StartInfo.RedirectStandardInput = true;
            gsflashGetMAC.StartInfo.RedirectStandardOutput = true;
            gsflashGetMAC.StartInfo.RedirectStandardError = true;
            gsflashGetMAC.StartInfo.CreateNoWindow = true;
            gsflashGetMAC.StartInfo.FileName = "gs_flashprogram.exe";
            try
            {
                comPort = myForm.GetCOMPort().Remove(0, 3);
            }
            catch
            {
                myForm.StatusError("No COM port selected!");
                return false;
            }
            gsflashGetMAC.StartInfo.Arguments = "-S" + comPort + " -r";

            try
            {
                gsflashGetMAC.Start();
            }
            catch
            {
                myForm.StatusError("gs_flashprogram.exe not found!");
                return false;
            }

            DateTime quitTime = DateTime.Now.AddSeconds(MACTIME);
            while (!gsflashGetMAC.HasExited && (DateTime.Now < quitTime)) ;

            if (gsflashGetMAC.HasExited)
            {
                string macPacket = gsflashGetMAC.StandardError.ReadToEnd();
                gsflashGetMAC.Close();
                string[] result = macPacket.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in result)
                {
                    if (s.StartsWith("MAC byte[2] = "))
                    {
                        string[] subString = s.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        MACAddress[0] = byte.Parse(subString[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    }
                    if (s.StartsWith("MAC byte[3] = "))
                    {
                        string[] subString = s.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        MACAddress[1] = byte.Parse(subString[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    }
                    if (s.StartsWith("MAC byte[4] = "))
                    {
                        string[] subString = s.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        MACAddress[2] = byte.Parse(subString[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    }
                    if (s.StartsWith("MAC byte[5] = "))
                    {
                        string[] subString = s.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        MACAddress[3] = byte.Parse(subString[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    }
                    if (s.StartsWith("MAC byte[6] = "))
                    {
                        string[] subString = s.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        MACAddress[4] = byte.Parse(subString[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    }
                    if (s.StartsWith("MAC byte[7] = "))
                    {
                        string[] subString = s.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        MACAddress[5] = byte.Parse(subString[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    }
                }
                macPacket = macPacket.Replace("\n", "\n\t");
                macPacket = macPacket.TrimEnd('\t');
                myForm.WriteSerialLog("\t" + macPacket);
                string MACString = "";
                int x;
                for (x = 0; x < 6; x++)
                {
                    MACString += Convert.ToString(MACAddress[x], 16).PadLeft(2,'0');
                    if (x < 5) MACString += "-";
                }
                myForm.WriteSerialLog("   *) Using detected MAC address: " + MACString + Environment.NewLine);
                return true;
            }
            else
            {
                gsflashGetMAC.Kill();
                gsflashGetMAC.Close();
                myForm.WriteSerialLog("   *) No response! Killing GS Flash Process..." + Environment.NewLine);
                return false;
            }
        }
        
        private bool StepFlashProgram()
        {
            gsflash = new Process();
            string comPort;

            gsflash.StartInfo.UseShellExecute = false;
            gsflash.StartInfo.RedirectStandardInput = true;
            gsflash.StartInfo.RedirectStandardOutput = true;
            gsflash.StartInfo.RedirectStandardError = true;  //use RedirectStandardOutput for most programs
            gsflash.StartInfo.CreateNoWindow = true;
            gsflash.StartInfo.FileName = "gs_flashloader.bat";

            try
            {
                comPort = myForm.GetCOMPort().Remove(0, 3);
            }
            catch
            {
                myForm.StatusError("No COM port selected!");
                return false;
            }
            gsflash.StartInfo.Arguments = comPort;
            
            try
            {
                gsflash.Start();
            }
            catch
            {
                myForm.StatusError("gs_flashloader.bat not found!");
                return false;
            }

            bool timeoutFlag = false;
            try { GsflashTimeoutThread.Join(); }
            catch { }
            GsflashTimeoutThread = new Thread(delegate() { timeoutFlag = GsflashTimeout(); });
            GsflashTimeoutThread.Start();

            char[] spinner = { '|','/','-','\\' };
            int x = 0;
            double percent = 0;
            int progressCount = 0;
            myForm.WriteSerialLog("   *) Flashing firmware..........");
            
            while (!gsflash.HasExited)
            {
                if (x == 4) x = 0;
                myForm.BackspaceSerialLogBox(7);
                myForm.WriteSerialLog(spinner[x].ToString() + percent.ToString("F1").PadLeft(5, ' ') + "%");
                Thread.Sleep(62);
                x++;
                percent += 0.2;
                if (percent > 100.0) percent = 100.0;
                progressCount++;
                if (progressCount > 9)
                {
                    progressCount = 0;
                    myForm.IncrementProgressBar();
                }
                //myForm.WriteSerialLog(((char)gsflash.StandardError.Read()).ToString());
            }

            myForm.BackspaceSerialLogBox(7);
            myForm.WriteSerialLog("." + percent.ToString("F1").PadLeft(5, ' ') + "%");
            
            GsflashTimeoutThread.Join();
            myForm.WriteSerialLog(Environment.NewLine);

            if (!timeoutFlag)
            {
                string versionPacket = gsflash.StandardOutput.ReadToEnd();
                string[] result = versionPacket.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in result)
                {
                    if (s.StartsWith("WF Burn File: "))
                    {
                        string[] subString = s.Split(new char[] { '-', '.' }, StringSplitOptions.RemoveEmptyEntries);
                        GSFirmwareVersion = subString[1].Replace('_', '.');
                    }
                    if (s.StartsWith("AF0 Burn File: "))
                    {
                        string[] subString = s.Split(new char[] { '-', '.' }, StringSplitOptions.RemoveEmptyEntries);
                        EvriskoFirmwareVersion = subString[1].Replace('_', '.');
                    }
                }
                versionPacket = versionPacket.Replace("\n", "\n\t");
                versionPacket = versionPacket.TrimEnd('\t');
                myForm.WriteSerialLog("\t" + versionPacket);
                string flashPacket = gsflash.StandardError.ReadToEnd();
                flashPacket = flashPacket.Replace("\n", "\n\t");
                flashPacket = flashPacket.TrimEnd('\t');
                myForm.WriteSerialLog("\t" + flashPacket);
                gsflash.Close();
                return true;
            }
            else
            {
                gsflash.Close();
                myForm.WriteSerialLog("   *) No response! Killing GS Flash Process..." + Environment.NewLine + Environment.NewLine);
                return false;
            }
        }

        private bool GsflashTimeout()
        {
            DateTime quitTime = DateTime.Now.AddSeconds(FLASHTIME);
            while (!gsflash.HasExited && (DateTime.Now < quitTime)) ;
            if (gsflash.HasExited) return false;
            else
            {
                gsflash.Kill();
                return true;
            }
        }

        private bool StepSetMAC()
        {
            //Note:  MAC address is in memory address 0x0801e800  (towards end of Bank 1 of flash)
            
            Thread.Sleep(1000);
            Process gsflashSetMAC = new Process();
            string comPort;
            string MACString = "";
            int x;
            //override to evrisko mac address
            MACAddress[0] = 0x74;
            MACAddress[1] = 0xd8;
            MACAddress[2] = 0x50;
            
            //MACString += "74d850";
            for (x = 0; x < 6; x++)
            {
                MACString += Convert.ToString(MACAddress[x], 16).PadLeft(2, '0');
            }

            gsflashSetMAC.StartInfo.UseShellExecute = false;
            gsflashSetMAC.StartInfo.RedirectStandardInput = true;
            gsflashSetMAC.StartInfo.RedirectStandardOutput = true;
            gsflashSetMAC.StartInfo.RedirectStandardError = true;
            gsflashSetMAC.StartInfo.CreateNoWindow = true;
            gsflashSetMAC.StartInfo.FileName = "gs_flashprogram.exe";
            try
            {
                comPort = myForm.GetCOMPort().Remove(0, 3);
            }
            catch
            {
                myForm.StatusError("No COM port selected!");
                return false;
            }
            gsflashSetMAC.StartInfo.Arguments = "-S" + comPort + " -m " + MACString;
            myForm.WriteSerialLog("   *) Setting MAC address..." + Environment.NewLine);
            try
            {
                gsflashSetMAC.Start();
            }
            catch
            {
                myForm.StatusError("gs_flashprogram.exe not found!");
                return false;
            }

            DateTime quitTime = DateTime.Now.AddSeconds(MACTIME);
            while (!gsflashSetMAC.HasExited && (DateTime.Now < quitTime)) ;

            if (gsflashSetMAC.HasExited)
            {
                string setMACPacket = gsflashSetMAC.StandardError.ReadToEnd();
                setMACPacket = setMACPacket.Replace("\n", "\n\t");
                setMACPacket = setMACPacket.TrimEnd('\t');
                myForm.WriteSerialLog("\t" + setMACPacket);
                gsflashSetMAC.Close();
                return true;
            }
            else
            {
                gsflashSetMAC.Kill();
                gsflashSetMAC.Close();
                myForm.WriteSerialLog("   *) No response! Killing GS Flash Process..." + Environment.NewLine);
                return false;
            }
        }

        private bool StepSaveConfig()
        {
            myForm.WriteSerialLog("   *) Writing config data into extended flash memory block..." + Environment.NewLine);
            bool error = false;

            myCOM_PORT = new SerialPort();
            try
            {
                myCOM_PORT.PortName = myForm.GetCOMPort();
                myCOM_PORT.BaudRate = 115200;
                myCOM_PORT.Parity = Parity.None;
                myCOM_PORT.DataBits = 8;
                myCOM_PORT.StopBits = StopBits.One;
                myCOM_PORT.ReadTimeout = 30;
                myCOM_PORT.WriteTimeout = 30;
                myCOM_PORT.Open();
            }
            catch
            {
                MessageBox.Show("Unable to open serial port!" + Environment.NewLine + "Ensure a valid COM port and baud rate is selected.");
                return false;
            }

            /*********This packet enables write operations on app flash 1(see register descriptions)****/
            myCOM_PORT.Write(new byte[] { 0xA5 }, 0, 1); //Start of Frame
            myCOM_PORT.Write(new byte[] { 0x07 }, 0, 1); //download class
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //reserved
            myCOM_PORT.Write(new byte[] { 0x00, 0x00 }, 0, 2); // add'l info
            myCOM_PORT.Write(new byte[] { 0x0A }, 0, 1); //length LSB
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //length MSB
            myCOM_PORT.Write(new byte[] { 0xEE }, 0, 1); //header checksum

            myCOM_PORT.Write(new byte[] { 0x02 }, 0, 1); //op code
            myCOM_PORT.Write(new byte[] { 0x18, 0x04, 0x00, 0x07 }, 0, 4); //address (little endian)
            myCOM_PORT.Write(new byte[] { 0x20 }, 0, 1); //size (number of bits to write--can be 8d 16d or 32d(0x08, 0x10, or 0x20))
            myCOM_PORT.Write(new byte[] { 0x44, 0x4C, 0x69, 0x57 }, 0, 4); //data to write (big endian)
            System.Threading.Thread.Sleep(5);

            WriteFlash(0x0801e808, MODULETYPE);   //Module Type (Sensor)
            WriteFlash(0x0801e809, (byte)subType); //Module Subtype (VTH)
            WriteFlash(0x0801e80a, GSMODEL);
			WriteFlash(0x0801e80b, GSCOMPAT);
			WriteFlash(0x0801e80c, Convert.ToByte(DateTime.Now.Year - 2000));   //Test Date Year (from 2000)
            WriteFlash(0x0801e80d, Convert.ToByte(DateTime.Now.Month));         //Test Date Month
            WriteFlash(0x0801e80e, Convert.ToByte(DateTime.Now.Day));           //Test Date Day
            WriteFlash(0x0801e80f, Convert.ToByte(DateTime.Now.Year - 2000));   //Update Date Year (from 2000)
            WriteFlash(0x0801e810, Convert.ToByte(DateTime.Now.Month));         //Update Date Month
            WriteFlash(0x0801e811, Convert.ToByte(DateTime.Now.Day));           //Update Date Day
            string[] eResult = EvriskoFirmwareVersion.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            WriteFlash(0x0801e812, byte.Parse(eResult[0]));                     //APP Firmware Major Version 
            WriteFlash(0x0801e813, byte.Parse(eResult[1]));                     //APP Firmware Minor Version
            WriteFlash(0x0801e814, byte.Parse(eResult[2]));                     //APP Firmware Minor Subversion
            string[] gResult = GSFirmwareVersion.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            WriteFlash(0x0801e815, byte.Parse(gResult[0]));                     //WLAN Firmware Major Version
            WriteFlash(0x0801e816, byte.Parse(gResult[1]));                     //WLAN Firmware Minor Version
            WriteFlash(0x0801e817, byte.Parse(gResult[2]));                     //WLAN Firmware Minor Subversion

            /*********This packet protects app flash 1 from writing****/
            myCOM_PORT.Write(new byte[] { 0xA5 }, 0, 1); //Start of Frame
            myCOM_PORT.Write(new byte[] { 0x07 }, 0, 1); //download class
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //reserved
            myCOM_PORT.Write(new byte[] { 0x00, 0x00 }, 0, 2); // add'l info
            myCOM_PORT.Write(new byte[] { 0x0A }, 0, 1); //length LSB
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //length MSB
            myCOM_PORT.Write(new byte[] { 0xEE }, 0, 1); //header checksum

            myCOM_PORT.Write(new byte[] { 0x02 }, 0, 1); //op code
            myCOM_PORT.Write(new byte[] { 0x18, 0x04, 0x00, 0x07 }, 0, 4); //address (little endian)
            myCOM_PORT.Write(new byte[] { 0x20 }, 0, 1); //size (number of bits to write--can be 8d 16d or 32d(0x08, 0x10, or 0x20))
            myCOM_PORT.Write(new byte[] { 0x00, 0x00, 0x00, 0x00 }, 0, 4); //data to write (big endian)
            System.Threading.Thread.Sleep(5);
            
            //Clear out the read buffer
            try
            {
                while (true) myCOM_PORT.ReadByte();
            }
            catch
            {

            }

            //Read everything back to verify flash memory
            if (ReadFlash(0x0801e808) != MODULETYPE) error = true;
            else if (ReadFlash(0x0801e809) != (byte)subType) error = true;
			else if (ReadFlash(0x0801e80a) != GSMODEL) error = true;
			else if (ReadFlash(0x0801e80b) != GSCOMPAT) error = true;
            else if (ReadFlash(0x0801e80c) != Convert.ToByte(DateTime.Now.Year - 2000)) error = true;
            else if (ReadFlash(0x0801e80d) != Convert.ToByte(DateTime.Now.Month)) error = true;
            else if (ReadFlash(0x0801e80e) != Convert.ToByte(DateTime.Now.Day)) error = true;
            else if (ReadFlash(0x0801e80f) != Convert.ToByte(DateTime.Now.Year - 2000)) error = true;
            else if (ReadFlash(0x0801e810) != Convert.ToByte(DateTime.Now.Month)) error = true;
            else if (ReadFlash(0x0801e811) != Convert.ToByte(DateTime.Now.Day)) error = true;
            else if (ReadFlash(0x0801e812) != byte.Parse(eResult[0])) error = true;
            else if (ReadFlash(0x0801e813) != byte.Parse(eResult[1])) error = true;
            else if (ReadFlash(0x0801e814) != byte.Parse(eResult[2])) error = true;
            else if (ReadFlash(0x0801e815) != byte.Parse(gResult[0])) error = true;
            else if (ReadFlash(0x0801e816) != byte.Parse(gResult[1])) error = true;
            else if (ReadFlash(0x0801e817) != byte.Parse(gResult[2])) error = true;
            else error = false;

            myCOM_PORT.Close();

            /****WRITE CONFIG DATA TO A BINARY FILE***/
            byte lrc = 0x00;
            byte cnt = 0x07;
            //calc checksum
            lrc ^= cnt;
            for (int m = 0; m < 6; m++)
            {
                lrc ^= MACAddress[m];
            }

            byte[] configData = new byte[] { lrc, cnt, MACAddress[0], MACAddress[1], MACAddress[2], MACAddress[3], MACAddress[4], MACAddress[5],
                                             MODULETYPE, (byte)subType, Convert.ToByte(DateTime.Now.Year - 2000), Convert.ToByte(DateTime.Now.Month),
                                             Convert.ToByte(DateTime.Now.Day), Convert.ToByte(DateTime.Now.Year - 2000), Convert.ToByte(DateTime.Now.Month),
                                             Convert.ToByte(DateTime.Now.Day), byte.Parse(eResult[0]), byte.Parse(eResult[1]), byte.Parse(eResult[2]),
                                             byte.Parse(gResult[0]), byte.Parse(gResult[1]), byte.Parse(gResult[2]) };
            
            string MACString = "";
            for (int x = 0; x < 6; x++)
            {
                MACString += Convert.ToString(MACAddress[x], 16).PadLeft(2, '0');
            }
            if (!Directory.Exists(@"VTH_CNF")) Directory.CreateDirectory(@"VTH_CNF");            
            string configFilename = @"VTH_CNF\";
            configFilename += MACString;
            string configFilenameExt = configFilename + ".cnf";
            int f = 2;
            while (File.Exists(configFilenameExt))
            {
                configFilenameExt = configFilename + "_v" + f.ToString() + ".cnf";
                f++;
            }
            BinaryWriter configWriter = new BinaryWriter(File.Create(configFilenameExt));
            configWriter.Write(configData);
            configWriter.Close();

            if (error == true) return false;
            else return true;

        }

        private void WriteFlash(int address32, byte dataByte) //Write
        {


            byte[] address = { 0x00, 0x00, 0x00, 0x00 };

            address[0] = (byte)address32;
            address[1] = (byte)(address32 >> 8);
            address[2] = (byte)(address32 >> 16);
            address[3] = (byte)(address32 >> 24);

            byte[] data = { 0xFF, 0xFF, 0xFF, 0xFF };

            //All bytes in the 32bit word are FF except the one for the desired address (0xFF does nothing to change flash contents)

            //figure out which byte of the 32 bit word the data goes into
            if ((address[0] & 0x03) == 0x00) data[0] = dataByte;
            if ((address[0] & 0x03) == 0x01) data[1] = dataByte;
            if ((address[0] & 0x03) == 0x02) data[2] = dataByte;
            if ((address[0] & 0x03) == 0x03) data[3] = dataByte;

            address[0] = (byte)(address[0] & 0xFC); //mask off lower two bits so we are at the beginning of a 32bit word



            /*********This packet tells the micro to write a 8bit word to memory****/
            myCOM_PORT.Write(new byte[] { 0xA5 }, 0, 1); //Start of Frame
            myCOM_PORT.Write(new byte[] { 0x07 }, 0, 1); //download class
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //reserved
            myCOM_PORT.Write(new byte[] { 0x00, 0x00 }, 0, 2); // add'l info
            myCOM_PORT.Write(new byte[] { 0x0A }, 0, 1); //length LSB
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //length MSB
            myCOM_PORT.Write(new byte[] { 0xEE }, 0, 1); //header checksum

            myCOM_PORT.Write(new byte[] { 0x02 }, 0, 1); //op code
            myCOM_PORT.Write(address, 0, 4); //address (little endian)
            myCOM_PORT.Write(new byte[] { 0x20 }, 0, 1); //size (number of bits to write--can be 8d 16d or 32d(0x08, 0x10, or 0x20))
            myCOM_PORT.Write(data, 0, 4); //data to write (big endian)

            System.Threading.Thread.Sleep(5);


        }

        private byte ReadFlash(int address32) //Read
        {

            byte[] address = { 0x00, 0x00, 0x00, 0x00 };

            address[0] = (byte)address32;
            address[1] = (byte)(address32 >> 8);
            address[2] = (byte)(address32 >> 16);
            address[3] = (byte)(address32 >> 24);

            /*********This packet tells the micro to read back a single word from memory****/
            myCOM_PORT.Write(new byte[] { 0xA5 }, 0, 1); //Start of Frame
            myCOM_PORT.Write(new byte[] { 0x07 }, 0, 1); //download class
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //reserved
            myCOM_PORT.Write(new byte[] { 0x00, 0x00 }, 0, 2); // add'l info
            myCOM_PORT.Write(new byte[] { 0x06 }, 0, 1); //length LSB
            myCOM_PORT.Write(new byte[] { 0x00 }, 0, 1); //length MSB
            myCOM_PORT.Write(new byte[] { 0xF2 }, 0, 1); //header checksum

            myCOM_PORT.Write(new byte[] { 0x01 }, 0, 1); //op code
            myCOM_PORT.Write(address, 0, 4); //address (little endian)
            myCOM_PORT.Write(new byte[] { 0x08 }, 0, 1); //size (number of bits to return--can be 8d 16d or 32d(0x08, 0x10, or 0x20))
            //this will always return a four bytes but the last bytes will be zeros if 8 or 16 bit word is selected above 
            //typical response (8bit):  A5 07 00 00 00 06 00 F2 11 04 33 00 00 00
            //typical response (16bit): A5 07 00 00 00 06 00 F2 11 04 33 07 00 00
            //typical response (32bit): A5 07 00 00 00 06 00 F2 11 04 33 07 FA CE
            //also the last bytes will wrap around if 32 bit word is selected (very confusing)
            //for instance if I read a 32 bit word from address 08 01 e8 01, I get "07 FA CE 33", which is address 0801e801, 0801e802, 0801e803, and 0801e800
            //to keep it simple, just use 8 bit words and discard the last 3 bytes

            byte[] output = new byte[262144]; // 256kB maximum size
            int x = 0;
            output[0] = (byte)myCOM_PORT.ReadByte();
            try
            {
                while (true)
                {
                    x++;
                    output[x] = (byte)myCOM_PORT.ReadByte();
                }
            }
            catch
            {

            }
            return output[10]; //reads only the first returned byte            
        }

        private bool StepCheckSensors()
        {
            myForm.WriteSerialLog("   *) Checking for proper connected sensors..." + Environment.NewLine);
            //Power Cycle gainspan into run mode
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(1, C_PGRM_EN);
            myUSB_SM_A.OpenRelay(1, C_VDD_3V3);
            Thread.Sleep(100);
            //Ground out power lines to make sure all caps are drained
            myUSB_SM_A.CloseRelay(R_GND, C_BATT);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_BATT);

            myUSB_SMU.SetVoltage(1, RUNBATTERY); //dual supplies to prepare for run mode
            myUSB_SMU.SetVoltage(2, RUNBATTERY);
            myUSB_SMU.SetVoltage(3, RUNBATTERY);
            //turn power back on
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);

            myCOM_PORT = new SerialPort();
            bool timedOut = false;

            try
            {
                myCOM_PORT.PortName = myForm.GetCOMPort();
                myCOM_PORT.BaudRate = int.Parse(myForm.GetBaudRate());
                myCOM_PORT.Parity = Parity.None;
                myCOM_PORT.DataBits = 8;
                myCOM_PORT.StopBits = StopBits.One;
                myCOM_PORT.Handshake = Handshake.None;
                myCOM_PORT.ReadTimeout = 10000;
                myCOM_PORT.WriteTimeout = 10000;
                myCOM_PORT.Open();
            }
            catch
            {
                myForm.StatusError("Unable to open serial port!" + Environment.NewLine + "Ensure a valid COM port and baud rate is selected.");
                return false;
            }
            myForm.WriteSerialLog("\t");

            //myUSB_SM_A.CloseRelay(R_GND, C_ALARM2);
            //Thread.Sleep(100);
            //myUSB_SM_A.OpenRelay(R_GND, C_ALARM2);
            if (!timedOut) timedOut = SerialCommand("", "TMP275 PRESENT: ");
            char inChar = '?';
            if (!timedOut) inChar = (char)myCOM_PORT.ReadByte();
            myForm.WriteSerialLog(inChar.ToString());
            if (subType != 2) if (inChar != '1') { myForm.WriteSerialLog(Environment.NewLine); return false; }
            
            if (!timedOut) timedOut = SerialCommand("", "HS01 PRESENT: ");
            if (!timedOut) inChar = (char)myCOM_PORT.ReadByte();
            myForm.WriteSerialLog(inChar.ToString());
            if (subType != 1) if (inChar != '1') { myForm.WriteSerialLog(Environment.NewLine); return false; }


                /*
                if (subType != 2)
                {
                    if (!timedOut) timedOut = SerialCommand("", "TMP275 PRESENT: 1");
                }
                else
                {
                    if (!timedOut) timedOut = SerialCommand("", "TMP275 PRESENT: 0");
                }
                if (subType != 1)
                {
                    if (!timedOut) timedOut = SerialCommand("", "HS01 PRESENT: 1");
                }
                else
                {
                    if (!timedOut) timedOut = SerialCommand("", "HS01 PRESENT: 0");
                }
                */
                if (timedOut)
                {

                    myForm.WriteSerialLog(Environment.NewLine + "   *) No Response! Closing Serial Port..." + Environment.NewLine);
                    myCOM_PORT.Close();

                    return false;
                }
                else
                {
                    myForm.WriteSerialLog(Environment.NewLine);
                    myCOM_PORT.Close();

                    return true;
                }
        }

        private bool StepSetupNode()
        {
            myForm.WriteSerialLog("   *) Configuring Node via Serial Connection..." + Environment.NewLine);
            /*
            //Power Cycle gainspan into run mode
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(1, C_PGRM_EN);
            myUSB_SM_A.OpenRelay(1, C_VDD_3V3);
            Thread.Sleep(100);
            //Ground out power lines to make sure all caps are drained
            myUSB_SM_A.CloseRelay(R_GND, C_BATT);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_BATT);

            myUSB_SMU.SetVoltage(1, RUNBATTERY); //dual supplies to prepare for run mode
            myUSB_SMU.SetVoltage(2, RUNBATTERY);
            myUSB_SMU.SetVoltage(3, RUNBATTERY);
            //turn power back on
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);
            */
            myCOM_PORT = new SerialPort();
            bool timedOut = false;

            try
            {
                myCOM_PORT.PortName = myForm.GetCOMPort();
                myCOM_PORT.BaudRate = int.Parse(myForm.GetBaudRate());
                myCOM_PORT.Parity = Parity.None;
                myCOM_PORT.DataBits = 8;
                myCOM_PORT.StopBits = StopBits.One;
                myCOM_PORT.Handshake = Handshake.None;
                myCOM_PORT.ReadTimeout = 10000;
                myCOM_PORT.WriteTimeout = 10000;
                myCOM_PORT.Open();
            }
            catch
            {
                myForm.StatusError("Unable to open serial port!" + Environment.NewLine + "Ensure a valid COM port and baud rate is selected.");
                return false;
            }
            myForm.WriteSerialLog("\t");

            myUSB_SM_A.CloseRelay(R_GND, C_ALARM2);
            Thread.Sleep(100);
            myUSB_SM_A.OpenRelay(R_GND, C_ALARM2);
            //Thread.Sleep(100);

            string shortMACString = "";
            int x;
            for (x = 3; x < 6; x++)
            {
                shortMACString += Convert.ToString(MACAddress[x], 16).PadLeft(2, '0');
            }

            if (!timedOut) timedOut = SerialCommand("", "> ");
            //if (!timedOut) timedOut = SerialCommand("M" + shortMACString + "\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("TMx" + Convert.ToString(MODULETYPE,16).PadLeft(2, '0') + "\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("TSx" + Convert.ToString((byte)subType,16).PadLeft(2, '0') + "\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("s" + SSID + "\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("c" + CHANNEL + "\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("w" + PASSPHRASE + "\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("i255.255.255.255\n", "> ");
            if (!timedOut) timedOut = SerialCommand("t1\n", "> ");
            //if (!timedOut) timedOut = SerialCommand("g", "> ");
            if (!timedOut) timedOut = SerialCommand("x", ": x");

            if (timedOut)
            {
                //myForm.BackspaceSerialLogBox(1);
                myForm.WriteSerialLog(Environment.NewLine + "   *) No Response! Closing Serial Port..." + Environment.NewLine);
                myCOM_PORT.Close();
                
                //Turn off
                myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
                Thread.Sleep(100);
                //Ground out power lines to make sure all caps are drained
                myUSB_SM_A.CloseRelay(R_GND, C_BATT);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_BATT);

                return false;
            }
            else
            {
                myForm.WriteSerialLog(Environment.NewLine);
                myCOM_PORT.Close();

                //Turn off
                myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
                Thread.Sleep(100);
                //Ground out power lines to make sure all caps are drained
                myUSB_SM_A.CloseRelay(R_GND, C_BATT);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_BATT);

                return true;
            }
        }

        private bool SerialCommand(string command, string prompt)
        {
            char inChar;
            bool timedOutOnce = false;
            bool timedOutTwice = false;

            serialResponse = "";
            myCOM_PORT.Write(command);// + Environment.NewLine);
            DateTime quitTime = DateTime.Now.AddSeconds(1);
            DateTime longQuitTime = DateTime.Now.AddSeconds(45);
            while ((DateTime.Now < longQuitTime) && (DateTime.Now < quitTime) && (!serialResponse.EndsWith(prompt)))
            {
                quitTime = DateTime.Now.AddSeconds(9.9);
                try
                {
                    inChar = (char)myCOM_PORT.ReadByte();
                    if (inChar == '\n') myForm.WriteSerialLog(Environment.NewLine + "\t");
                    else myForm.WriteSerialLog(inChar.ToString());
                    serialResponse += inChar.ToString();
                    //if (serialResponse.EndsWith("\r\n\r\n")) myForm.BackspaceSerialLogBox(3);
                }
                catch
                {
                    //Presses config button to try to wake up the stupid thing
                    if (!timedOutOnce)
                    {
                        quitTime = DateTime.Now.AddSeconds(9.9);
                        myCOM_PORT.Write("x\n");
                        Thread.Sleep(1000);
                        myUSB_SM_A.CloseRelay(4, 5);
                        Thread.Sleep(250);
                        myUSB_SM_A.OpenRelay(4, 5);
                        Thread.Sleep(250);
                        myUSB_SM_A.CloseRelay(4, 5);
                        Thread.Sleep(250);
                        myUSB_SM_A.OpenRelay(4, 5);
                        timedOutOnce = true;
                    }
                    else timedOutTwice = true;
                }
            }
            if (timedOutTwice) return true;
            else return false;

        }

        private bool SerialCommandNoEcho(string command, string prompt)
        {
            char inChar;

            serialResponse = "";
            myCOM_PORT.Write(command);// + Environment.NewLine);
            DateTime quitTime = DateTime.Now.AddSeconds(1);
            while ((DateTime.Now < quitTime) && (!serialResponse.EndsWith(prompt)))
            {
                quitTime = DateTime.Now.AddSeconds(9.9);
                try
                {
                    inChar = (char)myCOM_PORT.ReadByte();
                    //if (inChar == '\n') myForm.WriteSerialLog(Environment.NewLine + "\t");
                    //else myForm.WriteSerialLog(inChar.ToString());
                    serialResponse += inChar.ToString();
                    //if (serialResponse.EndsWith("\r\n\r\n")) myForm.BackspaceSerialLogBox(3);
                }
                catch { return true; }
            }
            return false;

        }

        private bool StepLEDs()
        {
            //turn power back on
            //Power Cycle gainspan into run mode
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SMU.Off();
            myUSB_SM_A.OpenRelay(1, C_PGRM_EN);
            myUSB_SM_A.OpenRelay(1, C_VDD_3V3);
            Thread.Sleep(100);
            //Ground out power lines to make sure all caps are drained
            myUSB_SM_A.CloseRelay(R_GND, C_BATT);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_BATT);

            myUSB_SMU.SetVoltage(1, RUNBATTERY); //dual supplies to prepare for run mode
            myUSB_SMU.SetVoltage(2, RUNBATTERY);
            myUSB_SMU.SetVoltage(3, RUNBATTERY);
            //turn power back on
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);

            myForm2 = new Form2(this);
            myForm2.ShowDialog();
            return LEDsResult;
        }
        
        private bool StepRetrievePacket()
        {

            getNode = new Process();
            string MACString = "";
            int x;
            MACString += "74d850";
            for (x = 3; x < 6; x++)
            {
                MACString += Convert.ToString(MACAddress[x], 16).PadLeft(2, '0');
            }
            char outputChar;
            string output = null;

            getNode.StartInfo.UseShellExecute = false;
            getNode.StartInfo.RedirectStandardInput = false;
            getNode.StartInfo.RedirectStandardOutput = true;
            getNode.StartInfo.RedirectStandardError = false;
            getNode.StartInfo.CreateNoWindow = true;
            getNode.StartInfo.FileName = "java.exe";
            getNode.StartInfo.Arguments = "-jar getNode.jar " + MACString;
            
            try
            {
                getNode.Start();
            }
            catch
            {
                myForm.StatusError("Java Runtime Environment and/or getNode.jar not found!");
                return false;
            }

            bool timeoutFlag = false;
            try { GetNodeTimeoutThread.Join(); }
            catch {}
            GetNodeTimeoutThread = new Thread(delegate() { timeoutFlag = GetNodeTimeout(); });
            GetNodeTimeoutThread.Start();

            myForm.WriteSerialLog("   *) Running getNode to listen for packets..." + Environment.NewLine);
            myForm.WriteSerialLog("\t");
            
            while (!getNode.HasExited)
            {          
                outputChar = (char)getNode.StandardOutput.Read();
                getNodePacket += outputChar.ToString();
                if (outputChar == (char)Keys.Back) myForm.BackspaceSerialLogBox(1);
                else if (outputChar == '\n') myForm.WriteSerialLog("\n\t");
                else myForm.WriteSerialLog(outputChar.ToString());   
            }
            //FakeTLVPacketThread.Join();
            GetNodeTimeoutThread.Join();
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            if (!timeoutFlag)
            {
                output = getNode.StandardOutput.ReadToEnd();
                getNodePacket += output;
                output = output.Replace("\n", "\n\t");
                output = output.TrimEnd('\t');
                myForm.WriteSerialLog(output);
                getNode.Close();
                return true;
            }
            else
            {
                getNode.Close();
                myForm.BackspaceSerialLogBox(2);
                myForm.WriteSerialLog("   *) No response! Killing Java GetNode Process..." + Environment.NewLine);
                return false;
            }
        }

        private bool GetNodeTimeout()
        {
            DateTime quitTime = DateTime.Now.AddSeconds(LISTENTIME);
            while (!getNode.HasExited && (DateTime.Now < quitTime)) ;
            if (getNode.HasExited) return false;
            else
            {
                getNode.Kill();
                return true;
            }
        }
       
        private double StepParseVoltage()
        {
            string[] result = getNodePacket.Split(new char[] { '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in result)
            {
                if (s.StartsWith("SensorList="))
                {
                    string[] sensorList = s.Split(new string[] { "[", "]", ", " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string t in sensorList)
                    {
                        if (t.StartsWith("Battery: "))
                        {
                            string[] subString = t.Split(new char[] { ' ', 'V' }, StringSplitOptions.RemoveEmptyEntries);
                            return double.Parse(subString[1]);
                        }
                    }
                }
            }
            return -999;
        }
        
        private double StepParseRSSI()
        {
            string[] result = getNodePacket.Split(new char[] { '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in result)
            {
                if (s.StartsWith("SensorList="))
                {
                    string[] sensorList = s.Split(new string[] { "[", "]", ", " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string t in sensorList)
                    {
                        if (t.StartsWith("RSSI: "))
                        {
                            string[] subString = t.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            return double.Parse(subString[1]);
                        }
                    }
                }
            }
            return -999;
        }

        private double StepParseTemperature()
        {
            string[] result = getNodePacket.Split(new char[] { '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in result)
            {
                if (s.StartsWith("SensorList="))
                {
                    string[] sensorList = s.Split(new string[] { "[", "]", ", " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string t in sensorList)
                    {
                        if (t.StartsWith("Temp: "))
                        {
                            string[] subString = t.Split(new char[] { ' ', 'C' }, StringSplitOptions.RemoveEmptyEntries);
                            return double.Parse(subString[1]);
                        }
                    }
                }
            }
            return 0;
        }

        private double StepParseHumidity()
        {
            string[] result = getNodePacket.Split(new char[] { '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in result)
            {
                if (s.StartsWith("SensorList="))
                {
                    string[] sensorList = s.Split(new string[] { "[", "]", ", " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string t in sensorList)
                    {
                        if (t.StartsWith("Humidity: "))
                        {
                            string[] subString = t.Split(new char[] { ' ', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            return double.Parse(subString[1]);
                        }
                    }
                }
            }
            return 0;
        }

        private bool StepAlarmPins()
        {
            myForm.WriteSerialLog("   *) Checking Both Alarm Inputs via Serial Connection..." + Environment.NewLine);

            //Power Cycle gainspan into run mode
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SM_A.OpenRelay(1, C_PGRM_EN);
            myUSB_SM_A.OpenRelay(1, C_VDD_3V3);
            Thread.Sleep(100);
            //Ground out power lines to make sure all caps are drained
            myUSB_SM_A.CloseRelay(R_GND, C_BATT);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_BATT);
            //turn power back on
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);

            myCOM_PORT = new SerialPort();
            bool timedOut = false;

            try
            {
                myCOM_PORT.PortName = myForm.GetCOMPort();
                myCOM_PORT.BaudRate = int.Parse(myForm.GetBaudRate());
                myCOM_PORT.Parity = Parity.None;
                myCOM_PORT.DataBits = 8;
                myCOM_PORT.StopBits = StopBits.One;
                myCOM_PORT.Handshake = Handshake.None;
                myCOM_PORT.ReadTimeout = 20000;
                myCOM_PORT.WriteTimeout = 20000;
                myCOM_PORT.Open();
            }
            catch
            {
                myForm.StatusError("Unable to open serial port!" + Environment.NewLine + "Ensure a valid COM port and baud rate is selected.");
                return false;
            }
            myForm.WriteSerialLog("\t");

            Thread.Sleep(2000);
            myUSB_SM_A.CloseRelay(4, 5);
            Thread.Sleep(250);
            myUSB_SM_A.OpenRelay(4, 5);
            if (!timedOut) timedOut = SerialCommand("", "Alarm: 0");

            Thread.Sleep(2000);
            myUSB_SM_A.CloseRelay(4, 6);
            Thread.Sleep(250);
            myUSB_SM_A.OpenRelay(4, 6);
            Thread.Sleep(250);
            myUSB_SM_A.CloseRelay(4, 6);
            Thread.Sleep(250);
            myUSB_SM_A.OpenRelay(4, 6);
            if (!timedOut) timedOut = SerialCommand("", "Alarm: 1");


            if (!timedOut) timedOut = SerialCommand("", "> ");
            if (!timedOut) timedOut = SerialCommand("t60\n", "> ");
            if (!timedOut) timedOut = SerialCommand("x", ": x");




            if (timedOut)
            {
                myForm.WriteSerialLog(Environment.NewLine + "   *) No Response! Closing Serial Port..." + Environment.NewLine);
                myCOM_PORT.Close();

                return false;
            }
            else
            {
                myForm.WriteSerialLog(Environment.NewLine);
                myCOM_PORT.Close();

                return true;
            }
        }

        private bool StepResetNode()
        {
            myForm.WriteSerialLog("   *) Resetting Node Configuration via Serial Connection..." + Environment.NewLine);

            //Power Cycle gainspan into run mode
            myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
            myUSB_SM_A.OpenRelay(1, C_PGRM_EN);
            myUSB_SM_A.OpenRelay(1, C_VDD_3V3);
            Thread.Sleep(100);
            //Ground out power lines to make sure all caps are drained
            myUSB_SM_A.CloseRelay(R_GND, C_BATT);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
            myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
            myUSB_SM_A.OpenRelay(R_GND, C_BATT);
            //turn power back on
            myUSB_SM_A.CloseRelay(R_SMU1_OP, C_BATT);

            myCOM_PORT = new SerialPort();
            bool timedOut = false;

            try
            {
                myCOM_PORT.PortName = myForm.GetCOMPort();
                myCOM_PORT.BaudRate = int.Parse(myForm.GetBaudRate());
                myCOM_PORT.Parity = Parity.None;
                myCOM_PORT.DataBits = 8;
                myCOM_PORT.StopBits = StopBits.One;
                myCOM_PORT.Handshake = Handshake.None;
                myCOM_PORT.ReadTimeout = 10000;
                myCOM_PORT.WriteTimeout = 10000;
                myCOM_PORT.Open();
            }
            catch
            {
                myForm.StatusError("Unable to open serial port!" + Environment.NewLine + "Ensure a valid COM port and baud rate is selected.");
                return false;
            }
            myForm.WriteSerialLog("\t");

            myUSB_SM_A.CloseRelay(R_GND, C_ALARM2);
            Thread.Sleep(100);
            myUSB_SM_A.OpenRelay(R_GND, C_ALARM2);

            if (!timedOut) timedOut = SerialCommand("", "> ");
            if (!timedOut) timedOut = SerialCommand("i255.255.255.255\n", "> ");
            if (!timedOut) timedOut = SerialCommand("t60\n", "> ");
            if (!timedOut) timedOut = SerialCommand("x", ": x");

            if (timedOut)
            {
                myForm.WriteSerialLog(Environment.NewLine + "   *) No Response! Closing Serial Port..." + Environment.NewLine);
                myCOM_PORT.Close();

                //Turn off
                myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
                Thread.Sleep(100);
                //Ground out power lines to make sure all caps are drained
                myUSB_SM_A.CloseRelay(R_GND, C_BATT);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_BATT);

                return false;
            }
            else
            {
                myForm.WriteSerialLog(Environment.NewLine);
                myCOM_PORT.Close();

                //Turn off
                myUSB_SM_A.OpenRelay(R_SMU1_OP, C_BATT);
                Thread.Sleep(100);
                //Ground out power lines to make sure all caps are drained
                myUSB_SM_A.CloseRelay(R_GND, C_BATT);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.CloseRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_1V8);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_3V3);
                myUSB_SM_A.OpenRelay(R_GND, C_VDD_5V);
                myUSB_SM_A.OpenRelay(R_GND, C_BATT);

                return true;
            }
        }

        private string GetIP()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        private string ToEngineeringNotation(double d, string format)  //Converts a double into a string in eng. notation
        {
            double exponent = Math.Log10(Math.Abs(d));
            if (Math.Abs(d) >= 1)
            {
                switch ((int)Math.Floor(exponent))
                {
                    case 0:
                    case 1:
                    case 2:
                        return d.ToString(format);
                    case 3:
                    case 4:
                    case 5:
                        return (d / 1e3).ToString(format) + "k";
                    case 6:
                    case 7:
                    case 8:
                        return (d / 1e6).ToString(format) + "M";
                    case 9:
                    case 10:
                    case 11:
                        return (d / 1e9).ToString(format) + "G";
                    case 12:
                    case 13:
                    case 14:
                        return (d / 1e12).ToString(format) + "T";
                    case 15:
                    case 16:
                    case 17:
                        return (d / 1e15).ToString(format) + "P";
                    case 18:
                    case 19:
                    case 20:
                        return (d / 1e18).ToString(format) + "E";
                    case 21:
                    case 22:
                    case 23:
                        return (d / 1e21).ToString(format) + "Z";
                    case 24:
                    case 25:
                    case 26:
                        return (d / 1e24).ToString(format) + "Y";
                    default:
                        return "Inft";
                }
            }
            else if (Math.Abs(d) > 0)
            {
                switch ((int)Math.Floor(exponent))
                {
                    case -1:
                    case -2:
                    case -3:
                        return (d * 1e3).ToString(format) + "m";
                    case -4:
                    case -5:
                    case -6:
                        return (d * 1e6).ToString(format) + "µ";
                    case -7:
                    case -8:
                    case -9:
                        return (d * 1e9).ToString(format) + "n";
                    case -10:
                    case -11:
                    case -12:
                        return (d * 1e12).ToString(format) + "p";
                    case -13:
                    case -14:
                    case -15:
                        return (d * 1e15).ToString(format) + "f";
                    case -16:
                    case -17:
                    case -18:
                        return (d * 1e18).ToString(format) + "a";
                    case -19:
                    case -20:
                    case -21:
                        return (d * 1e21).ToString(format) + "z";
                    case -22:
                    case -23:
                    case -24:
                        return (d * 1e24).ToString(format) + "y";
                    default:
                        return "Zero";
                }
            }
            else
            {
                return "0";
            }
        }

    }

}
