﻿using System;
using Ivi.Visa.Interop;
using MainForm;
using System.Threading;

namespace AgilentInstruments
{
    class USB_DMM
    {
        Form1 myForm;
        ResourceManager myRM;
        FormattedIO488Class myDMMConnection;

        const string NPLC = "10";        //Number of Power Line Cycles for measurements (lower is faster)
        const int sleep = 50;  //milliseconds to sleep before each measurement

        public USB_DMM(Form1 myFormHandle, ResourceManager myRMHandle) //Constructor 
        {
            myForm = myFormHandle;
            myRM = myRMHandle;
        }

        public bool Connect()
        {
            try
            {
                myDMMConnection = new FormattedIO488Class();
                myDMMConnection.IO = (IMessage)myRM.Open("USB_DMM", AccessMode.NO_LOCK, 2000, "");  //use DMM VISA Alias
                return true;
            }
            catch
            {
                myForm.StatusError("Unable to connect to DMM!" + Environment.NewLine + "Ensure it is on, connected, and aliased as USB_DMM.");
                return false;
            }
        }
        
        public bool Initialize()
        {
            myDMMConnection.WriteString("SYSTem:LFRequency 60", true);  //Set Line Frequency to 60Hz
            myDMMConnection.WriteString("SENSe:VOLTage:DC:NPLC " + NPLC, true); //Measurements require x Power Line Cycles
            myDMMConnection.WriteString("SENSe:VOLTage:DC:ZERO:AUTO OFF", true); //Disables autozero
            myDMMConnection.WriteString("SENSe:VOLTage:DC:RANGe:AUTO OFF", true); //Auto Ranging
            myDMMConnection.WriteString("SENSe:VOLTage:AC:NPLC " + NPLC, true); //Measurements require x Power Line Cycles
            myDMMConnection.WriteString("SENSe:VOLTage:AC:ZERO:AUTO OFF", true); //Disables autozero
            myDMMConnection.WriteString("SENSe:VOLTage:AC:RANGe:AUTO OFF", true); //Auto Ranging
            myDMMConnection.WriteString("SENSe:CURRent:DC:NPLC " + NPLC, true); //Measurements require x Power Line Cycles
            myDMMConnection.WriteString("SENSe:CURRent:DC:ZERO:AUTO OFF", true); //Disables autozero
            myDMMConnection.WriteString("SENSe:CURRent:DC:RANGe:AUTO OFF", true); //Auto Ranging
            myDMMConnection.WriteString("SENSe:RESistance:NPLC " + NPLC, true); //Measurements require x Power Line Cycles
            myDMMConnection.WriteString("SENSe:RESistance:ZERO:AUTO OFF", true); //Disables autozero
            myDMMConnection.WriteString("SENSe:RESistance:RANGe:AUTO OFF", true); //Auto Ranging
            myDMMConnection.WriteString("TRIGger:SOURce IMM", true); //Immediate measurements instead of bus triggering
            myDMMConnection.WriteString("CONFigure:VOLTage:DC 10, MAX", true); //return to voltage readings
            return true;
        }

        public double GetResistance()
        {
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:RESistance? AUTO, MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }
        
        public double GetResistance(int range)
        {
            myDMMConnection.WriteString("MEASure:RESistance? " + range.ToString() + ", MAX", true);
            double.Parse(myDMMConnection.ReadString());
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:RESistance? " + range.ToString() + ", MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public double GetVolts()
        {
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:VOLTage? AUTO, MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public double GetVolts(int range)
        {
            myDMMConnection.WriteString("MEASure:VOLTage? " + range.ToString() + ", MAX", true);
            double.Parse(myDMMConnection.ReadString());
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:VOLTage? " + range.ToString() + ", MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public double GetVoltsAC()
        {
            myDMMConnection.WriteString("MEASure:VOLTage:AC? ", true);
            double.Parse(myDMMConnection.ReadString());
            myDMMConnection.WriteString("MEASure:VOLTage:AC? ", true);
            double.Parse(myDMMConnection.ReadString());
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:VOLTage:AC? ", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public double GetVoltsAC(int range)
        {
            myDMMConnection.WriteString("MEASure:VOLTage:AC? " + range.ToString() + ", MAX", true);
            double.Parse(myDMMConnection.ReadString());
            myDMMConnection.WriteString("MEASure:VOLTage:AC? " + range.ToString() + ", MAX", true);
            double.Parse(myDMMConnection.ReadString());
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:VOLTage:AC? " + range.ToString() + ", MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public double GetAmps()
        {
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:CURRent? AUTO, MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public double GetAmps(double range)
        {
            myDMMConnection.WriteString("MEASure:CURRent? " + range.ToString() + ", MAX", true);
            double.Parse(myDMMConnection.ReadString());
            System.Threading.Thread.Sleep(sleep);
            myDMMConnection.WriteString("MEASure:CURRent? " + range.ToString() + ", MAX", true);
            return double.Parse(myDMMConnection.ReadString());
        }

        public void ConfigForVolts()
        {
            myDMMConnection.WriteString("CONFigure:VOLTage:DC 1000, MAX", true); //return to voltage readings
        }

        public void Close()
        {
            myDMMConnection.IO.Close();
        }
    }
}
