@ECHO OFF
REM Evrisko Systems, LLC - 2011
REM This batch file allows the loading of gs_flashprogram with custom firmware filenames
REM One argument is passed in: The number of the COM port (%1)

REM !!!Change filenames in this block only!!!
REM Version number must begin after the first hyphen (-) and end with a hyphen (-) or period (.)
REM Underscores in the version number will be converted to periods in the test software
REM Valid Examples: file_name_version-1_2_3.bin or filename-12_3.bin or filename-12_3-part1_bank1.bin
REM set wf_burnfile=wfw_REL-2_0_44.bin
REM set af0_burnfile=Evrisko_app1-1_07_00.bin
REM set af1_burnfile=Evrisko_app2-1_07_00.bin
for /f "delims=" %%f IN ('dir /b wfw*.bin') do set wf_burnfile=%%f
for /f "delims=" %%f IN ('dir /b Evrisko_app1*.bin') do set af0_burnfile=%%f
for /f "delims=" %%f IN ('dir /b Evrisko_app2*.bin') do set af1_burnfile=%%f

ECHO Evrisko Systems, LLC Flash Loader Script
ECHO WF Burn File: %wf_burnfile%
ECHO AF0 Burn File: %af0_burnfile%
ECHO AF1 Burn File: %af1_burnfile%
gs_flashprogram.exe -S%1 -v -ew -e0 -e1 -w %wf_burnfile% -0 %af0_burnfile% -1 %af1_burnfile%
